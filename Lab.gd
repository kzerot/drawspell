extends Spatial
tool
class_name Lab

const wall_inst = preload("res://Env/Wall.tscn")
const floor_inst= preload("res://Env/Floor.tscn")
const conn_inst= preload("res://Env/Connector.tscn")
const enter_inst= preload("res://Env/Enter.tscn")
export var width = 10
export var height = 10
export (bool) var reset  setget set_update
export var worm_length = 10
export var worm_split_length = 5
export var worms_count = 1
export var change_chance = 0.8
export var chance_split = 0.9
var array
var center = Vector2()
var available_points = []
var chest_points = []
var monsters_available = []
var monster_count = 0
var treasures_count = 0
var enter_point = null
var map = {
        "walls":[],
        "floors": [],
        "treasures": []
   }
enum {
    NONE=0,
    FREE=1,
    ENEMY=2,
    CHEST=3,
    ENTER=4
   }



func set_update(val):
    print("Update")
#    if Engine.editor_hint:
#        generate()

func generate():
    print("MAGIC: Start Generate")
    enter_point = null
    generate_difficulty()


    center = Vector2(int(width/2)- int(width/2)%2, int(height/2)- int(height/2)%2)
    for c in get_children():
        c.queue_free()
    print("Generate")
    var img = Image.new()
    array = []
    for w in width:
        array.append([])
        for h in height:
            array[w].append(0)

    # worm

    var w_pos = center
    var pos_to_cam = w_pos
    var prev = null
    var dir = Vector2(0,1)
    for i in range(4):
        worm(array, w_pos, worm_length, dir.rotated(deg2rad(90*i)), chance_split)


    available_points.shuffle()
    monsters_available.shuffle()
    var dead_ends = find_dead_ends()
    if dead_ends.size() > 0:
        if enter_point:
            array[enter_point.x][enter_point.y] = FREE
        enter_point = dead_ends.pop_back()
        map.enter = {"x":enter_point.x,"y":enter_point.y}
        array[enter_point.x][enter_point.y] = ENTER

    if not enter_point:
        enter_point = available_points.pop_front()
        array[enter_point.x][enter_point.y] = ENTER
        map.enter = {"x":enter_point.x,"y":enter_point.y}

    for x in range(width):
        for y in range(height):
            if array[x][y]:
                var a
                if array[x][y] == ENTER:
                    a = enter_inst.instance()
                    map.enter = {"x":x,"y":y}
                else:
                    a = floor_inst.instance()
                add_child(a)

                map.floors.append({"x":x,"y":y})
                a.translation = Vector3(x,0,y)* 8
                if not Engine.editor_hint:
                    var b = floor_inst.instance()
                    b.translation = Vector3(x,0,y)* 8
                    b.translation.y = 6
                    b.rotation_degrees.z = 180
                    add_child(b)

            if y < height-1 and ((array[x][y] and not array[x][y+1]) or (not array[x][y] and array[x][y+1])):
                var w = wall_inst.instance()
                add_child(w)
                w.translation = Vector3(x,0,y+0.5)* 8
                w.rotation_degrees.y = 90
                if array[x][y]:
                    map.walls.append({"x":0,"y":0.5, "angle":90, "base_x": x, "base_y": y})
                elif array[x][y+1]:
                    map.walls.append({"x":0,"y":-0.5, "angle":90, "base_x": x, "base_y": y+1})

            if x < width-1 and ((array[x][y] and not array[x+1][y]) or (not array[x][y] and array[x+1][y])):
                var w = wall_inst.instance()
                add_child(w)
                w.translation = Vector3(x+0.5,0,y)* 8
                if array[x][y]:
                    map.walls.append({"x":0.5,"y":0, "angle":0, "base_x": x, "base_y": y})
                elif array[x+1][y]:
                    map.walls.append({"x":-0.5,"y":0, "angle":0, "base_x": x+1, "base_y": y})


            if array[x][y] and (x == 0 or x == width-1):
                var w = wall_inst.instance()
                add_child(w)
                if x == 0:
                    map.walls.append({"x":-0.5,"y":0, "angle":0, "base_x": x, "base_y": y})
                    w.translation = Vector3(x-0.5,0,y)* 8
                else:
                    map.walls.append({"x":0.5,"y":0, "angle":0, "base_x": x, "base_y": y})
                    w.translation = Vector3(x+0.5,0,y)* 8

            if array[x][y] and (y == 0 or y == height-1):
                var w = wall_inst.instance()
                add_child(w)
                if y == 0:
                    map.walls.append({"x":0,"y":-0.5, "angle":90, "base_x": x, "base_y": y})
                    w.translation = Vector3(x,0,y-0.5)* 8
                else:
                    map.walls.append({"x":0,"y":+0.5, "angle":90, "base_x": x, "base_y": y})
                    w.translation = Vector3(x,0,y+0.5)* 8

                w.rotation_degrees.y = 90

            if y < height-1 and x < width-1:
                var c = array[x][y]
                var cr = array[x+1][y]
                var cd = array[x][y+1]
                var cdiag = array[x+1][y+1]
                if (c and cr and cd and not cdiag) or\
                   (not c and cr and cd and cdiag) or\
                   (c and not cr and cd and cdiag) or\
                   (c and cr and not cd and cdiag):
                    var column = conn_inst.instance()
                    add_child(column)
                    column.translation = Vector3(x+0.5,0,y+0.5)* 8



    monster_count = int(available_points.size()/3)



    return pos_to_cam

func find_dead_ends():
    var result = []
    var dirs = [Vector2(0,1),Vector2(0,-1),Vector2(1,0),Vector2(-1,0)]
    for x in width:
        for y in height:
            if not array[x][y]:
                continue
            var free_neighbors = 0
            for d in dirs:
                var r = Vector2(x+d.x, y+d.y)
                if r.x >=0 and r.y >=0 and r.x <width and r.y < height:
                    var neighbor = array[r.x][r.y]
                    if not neighbor:
                        free_neighbors += 1
                else:
                    free_neighbors += 1
            if free_neighbors == 3:
                result.append({"x": x, "y": y})
    result.shuffle()
    return result
#    print(result)

func get_monster():
    return monsters_available[randi()%monsters_available.size()]

func get_treasure():
    var value = int((g.dungeon_data.level/90.0)*5)
    if value > 0:
        value = randi()%value
    var t = data.treasures[value]
    var treasure = {}
    treasure.gold = t.gold + randi() % t.gold_variant
    treasure.potions = []
    var p_count = randi() % t.potions_count
    for p in p_count:
        treasure.potions.append(t.potions[randi()%t.potions.size()])

    if "item_chance" in t and t.item_chance >= randf():
        var shop = ShopGenerator.new()
        treasure.item = shop.generate_weapon(g.dungeon_data.level)

    return treasure

func generate_difficulty():
    if Engine.editor_hint:
        return
    # Difficulty from 0 to 100
    monsters_available = []
    for m in data.monsters:
        if g.dungeon_data.level >= m.min_level and g.dungeon_data.level <= m.max_level:
            monsters_available.append(m)
    if monsters_available.size() == 0:
        #All monsters are good if programmer is idiot
        for m in data.monsters:
             monsters_available.append(m)
    # From 3 on level 0 to 15 on level 100 -  DOESNT DOWRS
#    monster_count = get_from_min_max(3, 15)
    # NEW monster count, 1/4 of tiles are with monsters


    # Treasures, from 1 on level 0 to 5 on level 100
    treasures_count =get_from_min_max( 2, 6)
    if randf() > 0.8:
        treasures_count += 1
    # width and height  from 20 to 60
    width = get_from_min_max( 20, 30)
    height = get_from_min_max( 20, 30)
    # Worm length 10 - 35
    worm_length = get_from_min_max( 10, 20)
    # split 5 - 15
    worm_split_length = get_from_min_max( 5, 10)



func get_from_min_max(_min, _max):
    return _min + int((_max-_min) * g.dungeon_data.level / 100.0)

func worm(_array, start, _worm_length, dir, split_chance):
    var w_pos = start
    var rotated = false
    var splited = false

    for k in _worm_length:
        var what = FREE
        # Old version
#        if randf()> 0.8 and w_pos.distance_to(center) > 2:
#            what = ENEMY
        if w_pos.distance_to(center) > 2 and not w_pos in available_points:
            available_points.append(w_pos)
        if not _array[int(w_pos.x)][int(w_pos.y)] == ENTER:
            _array[int(w_pos.x)][int(w_pos.y)] = what

        if int(w_pos.x) %2 == 0 and int(w_pos.y) %2 == 0:
            if randf() > (change_chance if rotated else 0.5):
                dir = [Vector2(0,1),Vector2(0,-1),Vector2(1,0),Vector2(-1,0)][randi()%4]
                rotated = true
            elif randf() > (split_chance if splited else split_chance / 1.2):
                splited = true
                _array = worm(_array, w_pos, worm_split_length if _worm_length == worm_length else _worm_length / 2, dir.rotated(deg2rad(90)), split_chance+0.03)

        if (w_pos + dir).x >= 0 and (w_pos + dir).y >= 0 and (w_pos + dir).y < height and (w_pos + dir).x < width:
#            w_pos += dir
            pass
        else:
            if w_pos.x == 0 and dir.x == -1:
                dir.x = 1
            elif w_pos.x == width-1 and dir.x == 1:
                dir.x = -1

            if w_pos.y == 0 and dir.y == -1:
                dir.y = 1
            elif w_pos.y == height-1 and dir.y == 1:
                dir.y = -1

        if k == _worm_length-1 and not enter_point:
            if w_pos.distance_to(center) > 3:
                if enter_point:
                    if w_pos.distance_to(center) > enter_point.distance_to(center):
                        _array[int(enter_point.x)][int(enter_point.y)] = FREE
                        enter_point = w_pos
                        _array[int(w_pos.x)][int(w_pos.y)] = ENTER
                else:
                    enter_point = w_pos
                    _array[int(w_pos.x)][int(w_pos.y)] = ENTER
            elif w_pos.distance_to(center) > 2:
                chest_points.append(w_pos)



        w_pos += dir
    return _array

