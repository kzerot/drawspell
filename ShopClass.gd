extends Object
class_name ShopGenerator

var items = []

const weapon_damages = ["poison", "zap", "fire", "water"]
const damage_names = {
        "low": ["Weak", "Rusty", "Baby"],
        "medium": ["Medium", "Good", "Potent"],
        "height": ["Strong", "Forceful", "Cool"],
        "superior": ["Superior", "Godlike", "Demonic"]
   }
const weapon_template = {
        "damage": {},
        "name": "",
        "cost": 0,
        "type" : "staff"
   }



var staffs = [
        {"image": "res://UI/staffs/staff_fire.png", "model": "res://Weapons/StaffFire.tscn"},
        {"image": "res://UI/staffs/staff_poison.png", "model": "res://Weapons/StaffPoison.tscn"},
        {"image": "res://UI/staffs/staff_water.png", "model": "res://Weapons/StaffWater.tscn"},
        {"image": "res://UI/staffs/staff_zap.png", "model": "res://Weapons/StaffZap.tscn"},
    ]

func get_dmg_name(dmg):
    return damage_names[dmg][randi()%damage_names[dmg].size()]

func generate_weapon(level):
    var weapon = weapon_template.duplicate(true)
    var total_damage = clamp(level +  (randf()-0.5)*20, 0, 120)
    # Crit!
    if randf() > 0.95:
        total_damage *= 1.5
    total_damage = clamp(floor(total_damage), 1, 2000)
    var damage_number = clamp(1 + randi() % (1 + int(round(2 * float(level) /50))), 0, 4)
    var damages = weapon_damages.duplicate(true)
    var weapon_name = tr("staff")
    if total_damage > 0:
        weapon_name +=  " " + g.tr("of").replace("_","") + " "
        var damage_left = total_damage
        for i in damage_number:
            if not damage_left:
                break
            var current_damage = int(damage_left*randf())
            if i == damage_number-1:
                current_damage = damage_left
            damage_left -= current_damage
            if current_damage > 0:
                var damage_type = damages[randi()%damages.size()]
                damages.erase(damage_type)
                weapon.damage[damage_type] = current_damage
    else:
        weapon.descr = "Simple staff with no bonuses"

    if total_damage > 80:
        weapon_name = tr(get_dmg_name("superior")) + " " + weapon_name
    elif total_damage > 50:
        weapon_name = tr(get_dmg_name("height")) + " " + weapon_name
    elif total_damage > 20:
        weapon_name = tr(get_dmg_name("medium")) + " " + weapon_name
    else:
        weapon_name = tr(get_dmg_name("low")) + " " + weapon_name
    var i = 0
    if weapon.damage.size() > 0:
        weapon.descr = ""
    for w in weapon.damage:
        i+=1
        weapon_name += tr(w+ "_plural")
        weapon.descr += "+" + str(weapon.damage[w])+"% " + tr(w+"_damage") + "\n"
        if i < weapon.damage.size()-1:
            weapon_name += ", "
        elif i < weapon.damage.size():
            weapon_name += " " + tr("and") + " "


    weapon_name = weapon_name.replace("   ", " ")
    weapon_name = weapon_name.replace("  ", " ")

    weapon.name = weapon_name
    weapon.cost = 10 + int(total_damage * 2 * (1 + float(level)/100))
    var model_data = staffs[randi()%staffs.size()]
    weapon.model = model_data.model
    weapon.image = model_data.image


    print(weapon)
    return weapon

func init(level):
    for i in range(3):
        items.append(generate_weapon(level))
