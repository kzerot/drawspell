shader_type canvas_item;
//uniform sampler2D noise_tex : hint_albedo;
uniform vec4 edge_color : hint_color = vec4(0, 0,0,1);
uniform float outline_weight = 1.0;
uniform sampler2D pencil0;
uniform sampler2D pencil1;
uniform sampler2D pencil2;
uniform sampler2D pencil3;
void fragment(){
	float w = SCREEN_PIXEL_SIZE.x;
	float h = SCREEN_PIXEL_SIZE.y;
    
	vec4 n0 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2( -w, -h));
	vec4 n1 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2(0.0, -h));
	vec4 n2 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2(  w, -h));
	vec4 n3 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2( -w, 0.0));
//	vec4 n4 = texture(SCREEN_TEXTURE, SCREEN_UV);
	vec4 n5 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2(  w, 0.0));
	vec4 n6 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2( -w, h));
	vec4 n7 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2(0.0, h));
	vec4 n8 = texture(SCREEN_TEXTURE, SCREEN_UV + vec2(  w, h));

	vec4 sobel_edge_h = (n2 + (2.*n5) + n8 - (n0 + (2.*n3) + n6)) * outline_weight;
    vec4 sobel_edge_v = (n0 + (2.*n1) + n2 - (n6 + (2.*n7) + n8)) * outline_weight;
	vec4 sobel = sqrt((sobel_edge_h * sobel_edge_h) + (sobel_edge_v * sobel_edge_v));
    
    float alpha = (sobel.r  + sobel.g + sobel.b) / 3.0;


    vec4 col = texture(SCREEN_TEXTURE, SCREEN_UV);
    float weight = (col.r + col.g + col.b) / 3.0;
    vec4 pen_col = vec4(0);
    if(alpha >=0.3){
        pen_col = edge_color;
       }
    else if (weight >=0.5){
        pen_col = texture(pencil3, UV*2.0);
       }
    else if (weight >=0.3){
        pen_col = texture(pencil2, UV*2.0);
       }
    else if (weight >=0.2){
        pen_col = texture(pencil1, UV*2.0);
       }
    else if (weight >= 0.1){
        
        pen_col = texture(pencil0, UV*2.0);
       }
    else {
        pen_col =texture(pencil0, UV*2.0) * 0.3;
        
       }
    pen_col.a = 1.0;
    COLOR = mix(pen_col, col, 0.7);
//    COLOR = vec4(weight, weight, weight, 1.0);

//    COLOR =vec4( edge_color.rgb, alpha );
    
    
}