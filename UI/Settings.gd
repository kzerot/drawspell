extends Control

var locs = {
        "en": "English",
        "ru": "Русский",
        "el": "Ελληνικά",
        "de": "Deutsch",
        "fr": "Française"
   }
var loc_ids = ["en", "de", "fr", "ru", "el"]

func convert_locale(lo):
    var loc = lo.split("_")
    if loc in loc_ids:
        return loc
    return "en"

func _ready() -> void:
    print("MAGIC:"+ "Print locale:" , TranslationServer.get_locale())
    var stored_locale = get_setting("lang", "none", "lang")
    if not stored_locale or stored_locale == "none":
        stored_locale = convert_locale(TranslationServer.get_locale())
    if not stored_locale or not stored_locale in locs:
        stored_locale = "en"


    print("MAGIC:"+ "Set locale:" , stored_locale)
    TranslationServer.set_locale(stored_locale)
    $Language.text = locs[stored_locale]

    set_music(true)
    var t = get_tree().create_timer(1.0)
    t.connect("timeout", self, "connect_iap")
    var pop = $Language.get_popup()
    for i in loc_ids.size():
        pop.add_item(locs[loc_ids[i]], i)
#    pop.add_item(locs["ru"], 1)
    pop.connect("index_pressed", self, "lang_selected")
    print("MAGIC:"+ " Settings ready")

func lang_selected(index):
    var l = loc_ids[index]
    TranslationServer.set_locale(l)
    $Language.text = locs[l]
    set_setting("lang", l, "lang")

func connect_iap():
    print("MAGIC: Try to connect IAP")
    if not IAP.is_connected("purchase_success", self, "check_show_banner"):
        print("MAGIC: settings connect purchase_success")
        IAP.connect("purchase_success", self, "check_show_banner")

    if not IAP.is_connected("purchase_cancel", self, "refresh_banner_price"):
        IAP.connect("purchase_cancel", self, "refresh_banner_price")
        print("MAGIC: settings connect purchase_cancel")
    if not IAP.is_connected("purchase_owned", self, "refresh_banner_price"):
        IAP.connect("purchase_owned", self, "refresh_banner_price")
        print("MAGIC: settings connect purchase_owned")
    if not IAP.is_connected("purchase_fail", self, "refresh_banner_price"):
        IAP.connect("purchase_fail", self, "refresh_banner_price")
        print("MAGIC: settings connect purchase_fail")



func check_show_banner(item_name):
    print("MAGIC: Already pursuazed, settings ", item_name)
    if item_name == "remove_banner":
        $DisableAds.hide()

func refresh_banner_price():
    print("MAGIC: Refresh banner  button")
    $DisableAds.disabled = false
    if not g.show_banners or not g.banners or not g.banners.ready:
        $DisableAds.hide()
    elif g.remove_banner_price:
        $DisableAds.text = tr("Disads") + g.remove_banner_price
    else:
        $DisableAds.text = tr("Disable ads")


func _on_music(button_pressed: bool) -> void:
    var level = get_setting("music")
    for c in $Music/Volume.get_child_count():
        var s = $Music/Volume.get_child(c)
        if not button_pressed:
            s.pressed = false
        else:
            s.pressed = c <= level

    set_setting("music_mute", button_pressed)
    set_music()

func _on_effects(button_pressed: bool) -> void:
    var level = get_setting("effects")
    for c in $Effects/Volume.get_child_count():
        var s = $Effects/Volume.get_child(c)
        if not button_pressed:
            s.pressed = false
        else:
            s.pressed = c <= level

    set_setting("effects_mute", button_pressed)
    set_music()

func _on_sound(level: int) -> void:
    for c in $Music/Volume.get_child_count():
        var s = $Music/Volume.get_child(c)

        s.pressed = c <= level
    set_setting("music", level)
    set_setting("music_mute", true)
    $Music/CheckBox.pressed = true
    set_music()

func _on_effect(level: int) -> void:
    for c in $Effects/Volume.get_child_count():
        var s = $Effects/Volume.get_child(c)

        s.pressed = c <= level
    set_setting("effects", level)
    set_setting("effects_mute", true)
    $Effects/CheckBox.pressed = true
    set_music()

func set_setting(setting, value, section="audio"):
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    if err == OK:
        config.set_value(section, setting, value)
        config.save("user://settings.cfg")

func get_setting(setting, default=0, section="audio"):
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    var res
    if err == OK:
        if config.has_section_key(section, setting):
            res = config.get_value(section, setting)
        else:
            config.set_value(section, setting, default)
            res =  default
    else:
        res = default
    config.save("user://settings.cfg")

    return res

func set_music(update_controls=false):
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    if err != OK:
        config.save("user://settings.cfg")
        err = config.load("user://settings.cfg")
    if err == OK:
        var m_mute = false
        var e_mute = false
        var m_level = 2
        var e_level = 2
        if config.has_section_key("audio", "music_mute"):
            m_mute = !config.get_value("audio", "music_mute")
        if config.has_section_key("audio", "effects_mute"):
            e_mute = !config.get_value("audio", "effects_mute")
        if config.has_section_key("audio", "music"):
            m_level = config.get_value("audio", "music")
        if config.has_section_key("audio", "effects"):
            e_level = config.get_value("audio", "effects")

        var r_m_level = int_to_db(m_level)
        var r_e_level = int_to_db(e_level)

        AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), r_m_level)
        AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Effects"), r_e_level)
        AudioServer.set_bus_mute(AudioServer.get_bus_index("Music"), m_mute)
        AudioServer.set_bus_mute(AudioServer.get_bus_index("Effects"), e_mute)

        if update_controls:
            for c in $Effects/Volume.get_child_count():
                var s = $Effects/Volume.get_child(c)
                if e_mute:
                    s.pressed = false
                else:
                    s.pressed = c <= e_level

            for c in $Music/Volume.get_child_count():
                var s = $Music/Volume.get_child(c)
                if m_mute:
                    s.pressed = false
                else:
                    s.pressed = c <= m_level

            $Effects/CheckBox.pressed = !e_mute
            $Music/CheckBox.pressed = !m_mute

func int_to_db(i):
    match i:
        0:
            return -60
        1:
            return -33
        2:
            return -15
        3:
            return -5
        4:
            return 0


func _on_DisableAds_pressed() -> void:
    $DisableAds.disabled = true
    g.buy_ad_disable()


func _on_Settings_visibility_changed() -> void:
    refresh_banner_price()
