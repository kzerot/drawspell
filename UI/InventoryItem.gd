extends Control

export (NodePath) var name_node
export (NodePath) var descr_node
export (NodePath) var image_node
export (NodePath) var equip_node
export (NodePath) var equipped_node


signal equip(what)

var item_name
var descr
var image
var equip
var equipped

func equipped(is_equipped):
    equip.visible = !is_equipped
    equipped.visible = is_equipped

func _ready() -> void:
    item_name = get_node(name_node)
    descr = get_node(descr_node)
    image = get_node(image_node)
    equip = get_node(equip_node)
    equipped = get_node(equipped_node)



func set_item(_name, _descr, _image):
    item_name.text = _name
    descr.text = _descr
    image.texture = load(_image)



func _on_Pay_pressed() -> void:
    emit_signal("equip", self)
