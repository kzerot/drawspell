extends Popup

signal back


func _ready() -> void:
    $Settings.hide()
func _on_Settings_pressed() -> void:
    $Settings.show()


func _on_Menu_pressed() -> void:
    get_tree().change_scene("res://Menu.tscn")

func _on_Continue_pressed() -> void:
    emit_signal("back")
    hide()

func _on_Back_pressed() -> void:
    $Settings.hide()
