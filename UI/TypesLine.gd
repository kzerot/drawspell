extends HBoxContainer

func set_data(dmg):
    for n in get_children():
        n.hide()
    for d in dmg:
        if has_node(d.capitalize()):
            get_node(d.capitalize()).text = str(dmg[d])
            get_node(d.capitalize()).show()
            get_node(d.capitalize()+"Icon").show()
