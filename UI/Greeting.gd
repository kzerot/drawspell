extends Control

export (PoolStringArray) var screens
var screen = 0
var ready = true
signal closed
func _ready() -> void:
    for s in screens:
        $Cont.get_node(s).hide()

    $Cont.get_node(screens[0]).show()

func reset():
    ready = true

func _on_Greeting_gui_input(event: InputEvent) -> void:
    if (event is InputEventMouseButton or event is InputEventScreenTouch) and event.pressed and ready:
        $Cont.get_node(screens[screen]).hide()
        screen+=1
        if screens.size() >screen:
            $Cont.get_node(screens[screen]).show()
            ready = false
            var t = get_tree().create_timer(0.5)
            t.connect("timeout", self, "reset")
        else:
            self.hide()
            emit_signal("closed")
