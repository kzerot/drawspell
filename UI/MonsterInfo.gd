extends Popup

func set_data(enemy):
    $Cont/Top/HP.max_value = enemy.max_hp
    $Cont/Top/HP.value = enemy.HP
    $Cont/Top/Name.text = enemy.monster_name
    $Cont/Top/HP/HPText.text = str(enemy.HP) + "/" + str(enemy.max_hp)
    $Cont/Damage/Normal.text = str(enemy.damage)
    $Cont/Damage/Water.text = str(enemy.damage_water)
    $Cont/Damage/Fire.text = str(enemy.damage_fire)
    $Cont/Damage/Poison.text = str(enemy.damage_poison)
    $Cont/Damage/Zap.text = str(enemy.damage_zap)

    $Cont/Defence/Normal.text = str(enemy.defence)
    $Cont/Defence/Fire.text = str(enemy.defence_fire)
    $Cont/Defence/Water.text = str(enemy.defence_water)
    $Cont/Defence/Poison.text = str(enemy.defence_poison)
    $Cont/Defence/Zap.text = str(enemy.defence_zap)

    $Cont/Damage/Normal.visible = enemy.damage > 0
    $Cont/Damage/NormalIcon.visible = enemy.damage > 0
    $Cont/Damage/Water.visible = enemy.damage_water > 0
    $Cont/Damage/WaterIcon.visible = enemy.damage_water > 0
    $Cont/Damage/Fire.visible = enemy.damage_fire > 0
    $Cont/Damage/FireIcon.visible = enemy.damage_fire > 0
    $Cont/Damage/Poison.visible = enemy.damage_poison > 0
    $Cont/Damage/PoisonIcon.visible = enemy.damage_poison > 0
    $Cont/Damage/Zap.visible = enemy.damage_zap > 0
    $Cont/Damage/ZapIcon.visible = enemy.damage_zap > 0

    $Cont/Defence/Normal.visible = enemy.defence != 0
    $Cont/Defence/NormalIcon.visible = enemy.defence != 0
    $Cont/Defence/Water.visible = enemy.defence_water != 0
    $Cont/Defence/WaterIcon.visible = enemy.defence_water != 0
    $Cont/Defence/Fire.visible = enemy.defence_fire != 0
    $Cont/Defence/FireIcon.visible = enemy.defence_fire != 0
    $Cont/Defence/Poison.visible = enemy.defence_poison != 0
    $Cont/Defence/PoisonIcon.visible = enemy.defence_poison != 0
    $Cont/Defence/Zap.visible = enemy.defence_zap != 0
    $Cont/Defence/ZapIcon.visible = enemy.defence_zap != 0
