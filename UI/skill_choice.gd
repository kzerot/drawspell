extends TextureButton
var upgrade_name
export (String) var parent

export (int) var experience = 10

var enabled = false

func _ready() -> void:
    upgrade_name = name

func set_clickable():
    texture_normal = load("res://UI/skill_checked.png")
    enabled = true

func set_available():
    texture_normal = load("res://UI/skill_heighlight.png")

func set_disable():
    texture_normal = load("res://UI/skill_not_active.png")

