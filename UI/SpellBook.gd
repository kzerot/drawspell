extends Control
var added = []
const spell_inst = preload("res://UI/SpellInBook.tscn")
func _ready() -> void:
    refresh()

func refresh():
    added = []
    for c in $Spells/Cont.get_children():
        c.queue_free()

    for spell in g.effects:
        var skill_data = g.effects[spell]
        if true:
            added.append(spell)

            var sp = spell_inst.instance()
            $Spells/Cont.add_child(sp)
            if ResourceLoader.exists("res://UI/Symbols/" + spell+".png"):
                sp.get_node("Image").texture = load("res://UI/Symbols/" + spell+".png")
            var descr = ""

            if "damage" in skill_data:
                descr += "\n" + tr("Damage") + ": "
                var i = 0
                for d in skill_data.damage:
                    descr += str(skill_data.damage[d]) + " " + tr(d)
                    i += 1
                    if i < skill_data.damage.size():
                        descr+= ", "
            if "stat_up" in skill_data:
    #            var i = 0
                for d in skill_data.stat_up:
                    descr+= "\n"
                    descr += tr(d) + " +" + str(skill_data.stat_up[d])

            if "effect" in skill_data:
                match skill_data.effect.type:
                    "stun":
                        descr += "\n" + tr("stun_descr") % str(skill_data.effect.duration)
                    "dot":
                        for d in skill_data.effect.damage:
                            descr += "\n" + tr("dot") % [tr(d), str(skill_data.effect.damage[d]),
                                                        str(skill_data.effect.duration)]
            if "duration" in skill_data:
                descr+= "\n" + tr("Duration")+": " + str(skill_data.duration)+tr("s")
            if skill_data.type == "expertise":
                descr += "\n" + tr("expertise_descr")
            if "mana" in skill_data:
                descr+= "\n" + tr("Mana")+": " + str(skill_data.mana)
            sp.get_node("Descr/Descr").text = descr
            sp.get_node("Descr/Name").text = tr(spell)
