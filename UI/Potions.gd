extends ColorRect

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
const BASE_COUNT = 15
var slot_count = 0
var ph_instance = preload("res://UI/Placeholder.tscn")
var selected
var use_enabled = true
signal item_used(item)
#export (bool) var clickable = false

func _ready() -> void:
    create_slots(BASE_COUNT)

func create_slots(c):
    slot_count = 0
    for c in $Scroll/Grid.get_children():
        c.name += "old"
        c.queue_free()
    for i in BASE_COUNT:
        slot_count += 1
        var ph = ph_instance.instance()
        ph.name = "slot"+str(i)
        $Scroll/Grid.add_child(ph)
        ph.connect("pressed", self, "slot_pressed", [ph])

func fill_slots(items):
    # Clear
    for c in $Scroll/Grid.get_children():
        c.empty()

    for i in items.size():
        var item = items[i]
        if i < $Scroll/Grid.get_child_count():
            $Scroll/Grid.get_child(i).set_item(item)

func slot_pressed(slot):
    if slot.item:
        $Popup/Use.visible = use_enabled
        selected = slot.item
        $Popup/Name.text = selected.name
        if "descr" in selected.data:
            $Popup/Description.text = selected.data.descr
        $Popup/Image.texture = load(selected.data.image)
        $Popup.show_modal()

func _on_Use_pressed() -> void:
    if selected and selected.count > 0:
        emit_signal("item_used", selected)
        $Popup.hide()
        hide()
        get_tree().paused = false

func _on_Back_pressed() -> void:
    get_tree().paused = false
    hide()
