extends Control
tool

export (NodePath) var name_node
export (NodePath) var descr_node
export (NodePath) var image_node
export (NodePath) var count_node
export (NodePath) var pay_node
#export (NodePath) var back_color_node
#export (Color) var back_color setget set_bcolor, get_bcolor
#
#func set_bcolor(val):
#    if has_node(back_color_node):
#        get_node(back_color_node).color = val
#
#func get_bcolor():
#    if has_node(back_color_node):
#        return get_node(back_color_node).color
#    else:
#        return Color.black


var have_count = true

var item_name
var descr
var image
var count
var pay
const coin_inst = preload("res://UI/CoinsPay.tscn")
signal buy(who)

func _ready() -> void:
    item_name = get_node(name_node)
    descr = get_node(descr_node)
    image = get_node(image_node)
    pay = get_node(pay_node)

    if count_node and has_node(count_node):
        count = get_node(count_node)
    else:
        have_count = false

func set_item(_name, _descr, _image, _cost, _count=0):
    item_name.text = _name
    descr.text = _descr
    image.texture = load(_image)
    pay.text = str(_cost)
    if have_count and count:
        count.text = str(_count)

func commit():
    if have_count and count:
        count.text = str(int(count.text)+1)
    var c = coin_inst.instance()
    c.position = pay.rect_size/2
    pay.add_child(c)
func _on_Pay_pressed() -> void:
    emit_signal("buy", self)
