extends TextureButton

var item
var count

func empty():
    $Image.hide()
    $Count.hide()
    item = null
    count = 0

func set_item(_item):
    $Image.show()
    item = _item
    $Image.texture = load(item.data.image)
    set_count(item.count)

func set_count(_count):
    $Count.show()
    count = _count
    $Count.text = "x" + str(count)

func change_count(i):
    count += i
    if count < 0:
        count = 0
    set_count(count)
