extends ScrollContainer

signal deep
signal up
func _ready() -> void:
    start()

func _input(event: InputEvent) -> void:
    if (event is InputEventScreenTouch or event is InputEventMouseButton) and event.pressed:
        if $AnimationPlayer.is_playing():
            $AnimationPlayer.playback_speed = 3 if $AnimationPlayer.playback_speed == 1 else 1

func _on_Deep_pressed() -> void:
    emit_signal("deep")

func _on_Town_pressed() -> void:
    emit_signal("up")

func start():
    $AnimationPlayer.play("Start")
