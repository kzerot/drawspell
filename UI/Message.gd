extends Control
class_name Message

func start(txt, delta_pos=Vector2(0,0)):
    $Label.text = str(txt)
    $Label.set_anchors_preset(Control.PRESET_CENTER)
    $Label.rect_position += delta_pos
    $AnimationPlayer.play("Start")
