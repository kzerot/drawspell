extends HBoxContainer

func _on_music(button_pressed: bool) -> void:
    var level = get_setting("music")
    for c in $Settings/Music/Volume.get_child_count():
        var s = $Settings/Music/Volume.get_child(c)
        if not button_pressed:
            s.pressed = false
        else:
            s.pressed = c <= level

    set_setting("music_mute", button_pressed)

func _on_effects(button_pressed: bool) -> void:
    var level = get_setting("effects")
    for c in $Settings/Effects/Volume.get_child_count():
        var s = $Settings/Effects/Volume.get_child(c)
        if not button_pressed:
            s.pressed = false
        else:
            s.pressed = c <= level

    set_setting("effects_mute", button_pressed)

func _on_sound(level: int) -> void:
    for c in $Settings/Music/Volume.get_child_count():
        var s = $Settings/Music/Volume.get_child(c)

        s.pressed = c <= level
    set_setting("music", level)

func _on_effect(level: int) -> void:
    for c in $Settings/Effects/Volume.get_child_count():
        var s = $Settings/Effects/Volume.get_child(c)

        s.pressed = c <= level
    set_setting("effects", level)

func set_setting(setting, value, section="audio"):
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    if err == OK:
        config.set_value(section, setting, value)
        config.save("user://settings.cfg")

func get_setting(setting, default=0, section="audio"):
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    var res
    if err == OK:
        if config.has_section_key(section, setting):
            res = config.get_value(section, setting)
        else:
            config.set_value(section, setting, default)
            res =  default

    config.save("user://settings.cfg")

    return res

func set_music():
    var config = ConfigFile.new()
    var err = config.load("user://settings.cfg")
    if err == OK:
        var m_mute = false
        var e_mute = false
        var m_level = 4
        var e_level = 4
        if config.has_section_key("sound", "music_mute"):
            m_mute = config.get_value("sound", "music_mute")
        if config.has_section_key("sound", "effects_mute"):
            e_mute = config.get_value("sound", "effects_mute")
        if config.has_section_key("sound", "music"):
            m_level = config.get_value("sound", "music")
        if config.has_section_key("sound", "effects"):
            e_level = config.get_value("sound", "effects")

        m_level = int_to_db(m_level)
        e_level = int_to_db(e_level)

        AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), m_level)
        AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Effects"), e_level)
        AudioServer.set_bus_mute(AudioServer.get_bus_index("Music"), m_mute)
        AudioServer.set_bus_mute(AudioServer.get_bus_index("Effects"), m_mute)

func int_to_db(i):
    match i:
        0:
            return -60
        1:
            return -33
        2:
            return -15
        3:
            return -5
        4:
            return 0
