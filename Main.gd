extends Node

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var tween : Tween
var effect_tween : Tween
var battle_tween : Tween
var staff_tween : Tween
var lab
var x
var y
#var forw
var forw_x
var forw_y
var enemies = []
var base_staff_transform
var current_enemy : Enemy
var treasures = []
var spell # Current spell
var chest_found = 0
var enemies_killed = 0
var enemies_count = 0
var chests_count = 0
const msg_inst = preload("res://UI/Message.tscn")
onready var map = $UI/VC/VP/Map
const chest_inst = preload("res://Env/Chest/chest.tscn")
var spell_effect_id = 0
var ready_state = 0
var skills_in_progress = []
var skill_id = 0

func greet_ok():
    g.first_time.dungeon = false
    save()


func _ready() -> void:

    print("MAGIC: Start level")
    if g.first_time.dungeon:
        var greet = load("res://UI/GreetingBattle.tscn").instance()
        $UI.add_child(greet)
        greet.connect("closed", self, "greet_ok")

    $Input.game = self

    g.recalc_player()
    g.prepare_player()

    print("MAGIC: prepared")
    $UI/VC.visible = false
    $UI/Inventory.hide()
    $UI/NonBattle/TopButtons/Exp.text = str(g.player.experience) + " Exp"
    $UI/GoDeeper.hide()
    $UI/GoUp.hide()
    $UI/LevelUp.init(g.player)
    $UI/Inventory.init()
    $UI/HP.max_value = g.player.max_hp
    $UI/HP.value = g.player.hp
    $UI/Mana.max_value = g.player.max_mana
    $UI/Mana.value = g.player.mana
    $UI/Potions.connect("item_used", self, "use_potion")

    for c in $World/CameraPos/Camera/Weapon.get_children():
        c.queue_free()

    $World/CameraPos/Camera/Weapon.add_child(load(g.player.weapon.model).instance())

    effect_tween = Tween.new()
    add_child(effect_tween)

    effect_tween.connect("tween_all_completed", self, "effect_complete")

    battle_tween = Tween.new()
    add_child(battle_tween)
    battle_tween.connect("tween_all_completed", self, "battle_start")
    staff_tween = Tween.new()
    add_child(staff_tween)
    lab =  $World/Lab
    if not "seed" in g.dungeon_data:
        var seed_number = randi()
        g.dungeon_data.seed = seed_number

    seed(g.dungeon_data.seed)


    print("MAGIC: generaet labyrint")
    var cam_pos = lab.generate()
    lab.chest_points.shuffle()
    print("MAGIC: generaet treasures")
    if not "treasures" in g.dungeon_data:
        for i in lab.treasures_count:
            var point
            if lab.chest_points.size() > 0:
                point = lab.chest_points.pop_front()
                if point in lab.available_points:
                    lab.available_points.erase(point)
            elif lab.available_points.size() > 0:
                point = lab.available_points.pop_front()

            var x = point.x
            var y = point.y
            if lab.array[x][y] == Lab.ENTER:
                continue

            var chest =chest_inst.instance()
            $World/Enemy.add_child(chest)
            chest.translation = Vector3(x, 0, y) * 8

            lab.array[x][y] = lab.CHEST
            treasures.append({"chest": chest, "x": x, "y":  y})
        chests_count = treasures.size()
    else:
        treasures = []
        for e in g.dungeon_data.treasures:
            var x = e.x
            var y = e.y
            var chest = chest_inst.instance()
            $World/Enemy.add_child(chest)
            chest.translation = Vector3(x, 0, y) * 8

            lab.array[x][y] = lab.CHEST
            treasures.append({"chest": chest, "x": x, "y":  y})
        chests_count = g.dungeon_data.chests_count
        chest_found = g.dungeon_data.chest_found

    print("MAGIC: generaet monsters")
    if not "monsters" in g.dungeon_data:
        var points = lab.available_points
        points.shuffle()
        for i in range(lab.monster_count):
            if points.size() <= 0:
                break
            var point = points.pop_front()
            var x = point.x
            var y = point.y
            if lab.array[x][y] == Lab.ENTER:
                continue
            var monster = lab.get_monster()
            var enemy = monster.model.instance()
            $World/Enemy.add_child(enemy)
            enemy.translation = Vector3(x, 0, y) * 8
            enemies.append({"enemy": enemy, "x": x, "y":  y, "name": monster.name})
            lab.array[x][y] = lab.ENEMY
            enemy.connect("health_changed", self, "enemy_took_damage")
            enemy.connect("effect_cleared", self, "check_enemy_condition")

        g.dungeon_data.monsters = []
        for e in enemies:
            g.dungeon_data.monsters.append({"name": e.name, "x": e.x, "y": e.y})
        enemies_count = enemies.size()
    else:
        enemies = []
        for e in g.dungeon_data.monsters:
            var x = e.x
            var y = e.y
            var monster = data.get_monster(e.name)
            var enemy = monster.model.instance()
            $World/Enemy.add_child(enemy)
            enemy.translation = Vector3(x, 0, y) * 8
            enemies.append({"enemy": enemy, "x": x, "y":  y, "name": monster.name})
            lab.array[x][y] = lab.ENEMY
            enemy.connect("health_changed", self, "enemy_took_damage")
            enemy.connect("effect_cleared", self, "check_enemy_condition")
        enemies_count = g.dungeon_data.enemies_count
        enemies_killed = g.dungeon_data.enemies_killed

#    if "chest_found" in g.dungeon_data:
#        chest_found = g.dungeon_data.chest_found
#
#    if "enemies_killed" in g.dungeon_data:
#        enemies_killed = g.dungeon_data.enemies_killed
    print("Magic: refresh stats")
    refresh_stat()
    if "x" in g.dungeon_data and "y" in g.dungeon_data:
        cam_pos = Vector2(g.dungeon_data.x, g.dungeon_data.y)

    $World/CameraPos.translation.x = cam_pos.x * 8
    $World/CameraPos.translation.z = cam_pos.y * 8
    if "angle" in g.dungeon_data:
        $World/CameraPos.rotation_degrees.y = g.dungeon_data.angle
    x = int(cam_pos.x)
    y = int(cam_pos.y)
    $UI/Buttons/Up.connect("pressed", self, "button_pressed", ["up"])
#    $UI/Buttons/Down.connect("pressed", self, "button_pressed", ["down"])
    $UI/Buttons/Left.connect("pressed", self, "button_pressed", ["left"])
    $UI/Buttons/Right.connect("pressed", self, "button_pressed", ["right"])
    $UI/LevelUp.connect("recalc_player", self, "recalc")
    $UI/Died/ToTown.connect("pressed", self, "to_town")
    $UI/Died.hide()


    map.load_map(self)
    if "explored" in g.dungeon_data:
        map.load_explored(g.dungeon_data.explored)
    print("Magic: save")

    save()
    for e in enemies:
        if e.enemy.HP > 0:
            e.enemy.look_at($World/CameraPos.translation, Vector3.UP)
    base_staff_transform = $World/CameraPos/Camera/Weapon.transform

    check_forward()

    $Input.ready = true

    var b = g.banners
    if not g.show_banners:
        adjust_banner(0)
        b.show_banner(false)
    elif b and b.ready:
        b.connect("ad_ready", self, "adjust_banner")
        b.show_banner(true)
        adjust_banner(b.admob.getBannerHeight())
    g.wait_and_execute(0.5, self, "hide_loading")

func hide_loading():
    $UI/Loading.hide()

func adjust_banner(h):
    $UI.margin_bottom = -h
    $Black.rect_position.y = get_viewport().get_size_override().y - h
    print("MAGIC: ", $Black.rect_position.y )

func enemy_took_damage(hp):
    if current_enemy.HP <= 0:
        lab.array[forw_x][forw_y] = Lab.FREE
        end_battle()
    else:
        $UI/Battle/EnemyHP.value = hp


func save():
    g.dungeon_data.monsters = []
    g.dungeon_data.treasures = []
    g.dungeon_data.explored = []
    for m in  map.explored:
        g.dungeon_data.explored.append({"x": m.x, "y": m.y})

    for e in enemies:
        g.dungeon_data.monsters.append({"name": e.name, "x": e.x, "y": e.y})
    for t in treasures:
        g.dungeon_data.treasures.append({"x": t.x, "y": t.y})

    g.dungeon_data.x = x
    g.dungeon_data.y = y
    g.dungeon_data.chest_found = chest_found
    g.dungeon_data.enemies_killed = enemies_killed
    g.dungeon_data.chests_count = chests_count
    g.dungeon_data.enemies_count = enemies_count

    g.dungeon_data.angle = $World/CameraPos.rotation_degrees.y
    print("Saving")
    g.save("dungeon")

func to_town():
    g.player.hp = g.player.max_hp
    g.player.mana = g.player.max_mana
    $UI/Loading.show()
    g.wait_and_execute(0.2, g, "town")
#    g.town()

func use_potion(potion):
    var eff = g.use_potion(potion)
    for effect in eff.effects:
        spell_effect_id += 1
        var sp = effect.duplicate(true)
        sp.id = spell_effect_id
        if "duration" in sp:
            var timer = get_tree().create_timer(sp.duration)
            timer.connect("timeout", self, "clear_buff", [null,  spell_effect_id])

        g.player.effects.append(sp)
        check_player_condition()
    $Sounds.play("bottle")
    $UI/HP.value = g.player.hp
    $UI/Mana.value = g.player.mana

func short_msg(txt, dy=0, sound=null):
    var msg = msg_inst.instance()
    $UI/Messages.add_child(msg)
    msg.start(txt, Vector2(0, dy))
    if sound:
        $Sounds.play(sound)

func battle_start():
    current_enemy = get_enemy(x,y)
    if current_enemy and current_enemy.HP > 0:
        $UI/VC.visible = false
        $UI/NonBattle/Buttons/Map.pressed = false
        current_enemy.in_battle = true
        if not current_enemy.is_connected("bite", self, "take_damage"):
            current_enemy.connect("bite", self, "take_damage")
        $UI/Buttons.hide()
        $UI/NonBattle.hide()
        $UI/Battle.show()
        $Input.ready = true
        $UI/Battle/EnemyHP.max_value = current_enemy.max_hp
        $UI/Battle/EnemyHP.value = current_enemy.HP


        $UI/HP.max_value = g.player.max_hp
        $UI/HP.value = g.player.hp
        $UI/Mana.max_value = g.player.max_mana
        $UI/Mana.value = g.player.mana

        print("Enemy:", current_enemy.name)
    else:
        $UI/Buttons.show()
#        $UI/Buttons/Up.show()

        $UI/NonBattle.show()
        $UI/Battle.hide()
#        $Input.ready = false
        $Input.end()
        check_forward()

func take_damage():
    if current_enemy:
        var res = g.player.take_damage(current_enemy.get_damage())
        $UI/Blood.hit()
        $UI/HP.value = g.player.hp

        short_msg("-"+str(res.total)+ " HP", -150)

        if g.player.hp <=0:
            end_battle()

func get_enemy(x,y):
    for e in enemies:
        if e.x == forw_x and e.y == forw_y:
            return e.enemy

func start_battle():
    battle_tween.interpolate_property($World/CameraPos/Camera, "translation",
                    $World/CameraPos/CameraPos.translation,
                    $World/CameraPos/BattlePos.translation, 0.3,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    battle_tween.start()

func end_battle():
    if g.player.hp > 0: # WIN
        battle_tween.interpolate_property($World/CameraPos/Camera, "translation",
                        $World/CameraPos/BattlePos.translation,
                        $World/CameraPos/CameraPos.translation, 0.3,
                        Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
        battle_tween.start()
        g.player.experience += current_enemy.experience
        var gold = current_enemy.get_gold()
        g.player.gold += gold
        var t1 = get_tree().create_timer(0.5)
        var t2 = get_tree().create_timer(1.0)
        t1.connect("timeout", self, "short_msg", ["+"+str(current_enemy.experience)+" Exp"])
        t2.connect("timeout", self, "short_msg", ["+"+str(gold)+" gold", 50, "coin"])

        erase_enemy(current_enemy)
        enemies_killed += 1
        $UI/NonBattle/TopButtons/Exp.text = str(g.player.experience)+" Exp"

        save()

        return_staff()
        current_enemy = null
        $Input.ready = true
        refresh_stat()
    else: # LOSE
        if current_enemy:
            current_enemy.in_battle = false
        $UI/Died.show()
        $UI/Died/AnimationPlayer.play("Start")

func erase_enemy(enemy):
    var n = -1
    for i in enemies.size():
        var en = enemies[i]
        if en.enemy == enemy:
            lab.array[en.x][en.y] = Lab.FREE
            n = i
            break
    if n >= 0:
        enemies.remove(n)

func erase_chest(x,y):
    var n = -1
    for i in treasures.size():
        var en = treasures[i]
        if en.x == x and en.y == y:
            n = i
            en.chest.queue_free()
            break
    if n >= 0:
        treasures.remove(n)
    map.delete_treasure(x,y)

func button_pressed(btn):
    $UI/Buttons.hide()
    if not tween:
        tween = Tween.new()
        add_child(tween)
        tween.connect("tween_all_completed", self, "tween_complete")

    match btn:
        "up":
            tween.interpolate_property($World/CameraPos, "translation",
                    $World/CameraPos.translation,
                    $World/CameraPos.translation + Vector3(0,0,-8).rotated(Vector3.UP, $World/CameraPos.rotation.y), 0.7,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
            $Sounds.play("steps")

        "down":
            tween.interpolate_property($World/CameraPos, "translation",
                    $World/CameraPos.translation,
                    $World/CameraPos.translation + Vector3(0,0,8).rotated(Vector3.UP, $World/CameraPos.rotation.y), 0.7,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
            $Sounds.play("steps")

        "left":
            tween.interpolate_property($World/CameraPos, "rotation",
                    $World/CameraPos.rotation,
                    $World/CameraPos.rotation + Vector3(0, deg2rad(90), 0), 0.5,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)

        "right":
            tween.interpolate_property($World/CameraPos, "rotation",
                    $World/CameraPos.rotation,
                    $World/CameraPos.rotation - Vector3(0, deg2rad(90), 0), 0.5,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)

    tween.start()

func get_angle(rad=true):
    if rad:
        return deg2rad(-round($World/CameraPos.rotation_degrees.y))
    else:
        return -round($World/CameraPos.rotation_degrees.y)

func check_forward():
    x = int(round($World/CameraPos.translation.x / 8))
    y = int(round($World/CameraPos.translation.z / 8))
    get_forw()
    $UI/Buttons/Up.visible = lab.array[forw_x][forw_y] in [lab.FREE, lab.CHEST, lab.ENTER]

func tween_complete():
    $Sounds.stop("steps")
    $UI/Buttons.show()
    x = int(round($World/CameraPos.translation.x / 8))
    y = int(round($World/CameraPos.translation.z / 8))
    map.update()
    print("Pos: ",x," ", y)
    get_forw()

    if forw_x > 0 and forw_y > 0 and forw_x < lab.width and forw_y < lab.height:

        $UI/Buttons/Up.visible = lab.array[forw_x][forw_y] in [lab.FREE, lab.CHEST, lab.ENTER]
        if not get_enemy(x,y) and lab.array[forw_x][forw_y] == Lab.ENEMY:
            lab.array[forw_x][forw_y] = Lab.FREE
            $UI/Buttons/Up.visible = true
    else:
        $UI/Buttons/Up.visible = false
    for e in enemies:
        if e.enemy.HP > 0:
            e.enemy.look_at($World/CameraPos.translation, Vector3.UP)
    print(x, " ", y, " ", " what = ", lab.array[x][y])
    $UI/GoDeeper.hide()
    if lab.array[x][y] == Lab.ENTER:
        if g.dungeon_data.level == 100 and not g.won:
            var end = load("res://UI/Creds.tscn").instance()
            $UI.add_child(end)
            end.connect("deep", g, "deeper")
            end.connect("up", self, "up_with_level")
            end.start()
            g.won = true
            save()
        else:
            $UI/GoDeeper.show()
    elif lab.array[x][y] == Lab.CHEST:
        lab.array[x][y] = Lab.FREE
        var treasure = lab.get_treasure()
        var dy = -100
        g.player.gold += treasure.gold
        short_msg(str(treasure.gold) + " " + tr("Gold"), dy)
        $Sounds.play("coin")
        for p in treasure.potions:
            g.player.change_item_count("potions", p, 1)
            dy += 50
            short_msg(tr(p), dy)
        if "item" in treasure and g.player.has_slot():
            g.player.inventory.append(treasure.item)
            short_msg(treasure.item.name, dy)
        erase_chest(x,y)
        chest_found += 1
        refresh_stat()
    if forw_x > 0 and forw_y > 0 and forw_x < lab.width and forw_y < lab.height and \
            lab.array[forw_x][forw_y] == Lab.ENEMY:
        $UI/NonBattle.hide()
        start_battle()
    else:
        save()
#    if back.x > 0 and back.y > 0 and back.x < lab.width and back.y < lab.height:
#        $UI/Buttons/Down.visible = lab.array[back.x][back.y] == 1


func check_all_clear():
    if enemies_killed == enemies_count and chest_found == chest_found:
        g.player.gold += 10
        g.player.expirience += 5 + int(g.dungeon_data.level / 10.0)
        short_msg("Bonus gold: " + str(10), -300)
        short_msg("Bonus expirience: " + str(5 + int(g.dungeon_data.level / 10.0)), -250)

func up_with_level():
    g.max_level += 1
    g.town()
var mana_tick = 0
func _process(delta: float) -> void:
    mana_tick += delta

    #  skills_in_progress.size() == 0  and
    if g.player.mana <= g.player.max_mana and (is_instance_valid(current_enemy) and current_enemy.in_battle):
        g.player.mana = clamp(g.player.mana+2*delta, 0, g.player.max_mana)
        $UI/Mana.value = g.player.mana
    elif g.player.mana < g.player.max_mana  and not current_enemy:
        g.player.mana = clamp(g.player.mana+10*delta, 0, g.player.max_mana)
        $UI/Mana.value = g.player.mana

    if mana_tick >= 1:
        mana_tick = 0




        for e in g.player.effects:
            if e.type == "dot":
                g.player.take_damage(e.damage)
            elif e.type == "regen_hp":
                g.player.hp = clamp(g.player.hp + e.value, 0, g.player.max_hp)
            elif e.type == "regen_mana":
                g.player.mana = clamp(g.player.mana + e.value, 0, g.player.max_mana)

        $UI/Mana.value = g.player.mana
        $UI/HP.value = g.player.hp
        if is_instance_valid(current_enemy):
            $UI/Battle/EnemyHP.value = current_enemy.HP

func check_enemy_condition():
    for c in $UI/Battle/DOT.get_children():
        c.hide()
    if is_instance_valid(current_enemy):
        for c in current_enemy.effects:
            var cond = current_enemy.effects[c]
            if cond.type == "dot":
                for d in cond.damage:
                    match d:
                        "poison":
                            $UI/Battle/DOT/Poison.show()
                        "fire":
                            $UI/Battle/DOT/Fire.show()
            elif cond.type == "stun":
                $UI/Battle/DOT/Stun.show()

func check_player_condition():
    for c in $UI/PCond.get_children():
        c.hide()

    for c in g.player.effects:
        var cond = c
        if cond.type == "dot":
            for d in cond.damage:
                match d:
                    "poison":
                        $UI/DOT/Poison.show()
                    "fire":
                        $UI/DOT/Fire.show()
        elif cond.type in ["buff", "pot_buff"]:
            if "stat_up" in cond:
                for st in cond.stat_up:
                    match st:
                        "defence":
                            $UI/PCond/Shield.show()
                        "defence_water":
                            $UI/PCond/ShieldWater.show()
                        "defenced_fire":
                            $UI/PCond/ShieldFire.show()
                        "defence_zap":
                            $UI/PCond/ShieldZap.show()
                        "defence_poison":
                            $UI/PCond/ShieldPoison.show()
        elif cond.type == "regen_hp":
            $UI/PCond/RegenHp.show()
        elif cond.type == "regen_mana":
            $UI/PCond/RegenMana.show()
func refresh_stat():
    $UI/NonBattle/StatLevel/Chests/Val.text = str(chest_found) + "/" + str(chests_count)
    $UI/NonBattle/StatLevel/Enemies/Val.text = str(enemies_killed) + "/" + str(enemies_count)

func effect_complete(effect=null):

    $Input.ready = true
    if effect and effect in skills_in_progress:
        skills_in_progress.erase(effect)
    if current_enemy and is_instance_valid(current_enemy) and current_enemy.in_battle:
        if spell:
            if spell.type == "missile":
                var was_crit = g.player.is_crit()

                var dmg = []
                for d in spell.damage:
                    var mod = 0
                    if g.player.weapon and d in g.player.weapon.damage:
                        mod += g.player.weapon.damage[d]/100.0
                    if was_crit:
                        mod += g.player.crit / 100.0
                    dmg.append({
                        "dmg": spell.damage[d] * (1 + g.player.power/100.0 + mod) ,
                        "type": d
                       })

                var result = current_enemy.take_damage(dmg)

                if "effect" in spell:
                    current_enemy.add_effect(spell.effect)

#                short_msg(tr(spell.name), -50)
                short_msg(("" if not was_crit else "Crit!") + " -"+str(result.total)+ " HP")
        $UI/Battle/EnemyHP.value = current_enemy.HP
        check_enemy_condition()
        if current_enemy.HP <= 0:
            lab.array[forw_x][forw_y] = Lab.FREE
            end_battle()


func return_staff():
    staff_tween.interpolate_property($World/CameraPos/Camera/Weapon, "transform",
                    $World/CameraPos/Camera/Weapon.transform,
                    base_staff_transform, 0.3,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    staff_tween.start()


func _on_Input_casted(_spell, success) -> void:
    if _spell == "cheat":
        short_msg("CHEAT UNLOCKED")
        g.player.gold += 1000
        g.player.experience += 5000
        g.max_level = max(100, g.max_level)
        return
    if not _spell in g.effects:
        return # Not learned yet
    spell = g.effects[_spell]
    if not "name" in spell:
        spell.name = _spell

    if g.player.mana >= spell.mana:

        short_msg(tr(spell.name), -200)
        g.player.mana -= spell.mana
        $UI/Mana.value = g.player.mana
        if spell.type == "missile":
            var id = skill_id
            skill_id += 1
            var fx = spell.model.instance()
            $World/Effects.add_child(fx)
            var fire_point = $World/CameraPos/Shooting.global_transform.origin
            if not current_enemy or not is_instance_valid(current_enemy) or not current_enemy.in_battle:
                fire_point = $World/CameraPos/ShootingOffBattle.global_transform.origin
#            fire_point = $World/CameraPos/Camera/Weapon.get_child(0).fire_point.global_transform.origin

            fx.translation = fire_point
            fx.rotation = $World/CameraPos.rotation
            fx.connect("hit", self, "effect_complete", [skill_id])
            fx.fire()
            $Input.ready = true
            skills_in_progress.append(skill_id)

        elif spell.type == "buff":
            var fx = spell.model.instance()
            var id = spell_effect_id
            spell_effect_id += 1
            spell = spell.duplicate(true)
            spell.id = id

            g.player.effects.append(spell)

            $World/Effects.add_child(fx)
            fx.translation = $World/CameraPos/BattlePos.global_transform.origin
            fx.rotation = $World/CameraPos.rotation
            fx.fire()
            if "duration" in spell:
                var timer = get_tree().create_timer(spell.duration)
                timer.connect("timeout", self, "clear_buff", [fx,  id])

            check_player_condition()
            $Input.ready = true
        elif spell.type == "expertise":
            if not current_enemy or not is_instance_valid(current_enemy) or not current_enemy.in_battle:
                return
            get_tree().paused = true

            $UI/MonsterInfo.show_modal()
            $UI/MonsterInfo.set_data(current_enemy)
            $Input.ready = true
    else:

        short_msg(tr("no_mana"), -200)


func recalc():
    g.recalc_player()
    $UI/HP.max_value = g.player.max_hp
    $UI/HP.value = g.player.hp
    $UI/Mana.max_value = g.player.max_mana
    $UI/Mana.value = g.player.mana
    $UI/NonBattle/TopButtons/Exp.text = str(g.player.experience) + " Exp"

func clear_buff(fx, spell_id):
    if fx and fx.has_method("die"):
        fx.die()
    var to_remove
    for s in g.player.effects.size():
        if g.player.effects[s].id == spell_id:
            g.player.effects.remove(s)
            break
#    if spell_id in g.player.effects:
#        g.player.effects.erase(spell_id)
    check_player_condition()

func _on_MenuUpgrade_pressed() -> void:
    $UI/LevelUp.upd_exp()
    $UI/LevelUp.show()


func _on_Potions_pressed() -> void:
    get_tree().paused = true
    $UI/Potions.fill_slots(g.player.get_potions())
    $UI/Potions.show()


func get_forw():
    var dir = Vector2(0, 1).rotated(deg2rad(-round($World/CameraPos.rotation_degrees.y)))
    var forw = Vector2(x,y) - dir
    forw_x = int(round(forw.x))
    forw_y = int(round(forw.y))


func _on_Stay_pressed() -> void:
    $UI/GoDeeperMessage.hide()
    $UI/GoUp.hide()
    get_forw()
    if lab.array[forw_x][forw_y] == Lab.ENEMY:
        start_battle()

func _on_Deeper_pressed() -> void:

    $UI/Loading.show()
    g.wait_and_execute(0.2, g, "deeper")
#    g.deeper()


func _on_Exit_pressed() -> void:
    $UI/GoUp.show()


func _on_Up_pressed() -> void:
    g.town()


func _on_Inventory_pressed() -> void:
    $UI/Inventory.show()
    $UI/Inventory.refresh()


func _on_Inventory_weapon_changed() -> void:
    for c in $World/CameraPos/Camera/Weapon.get_children():
        c.queue_free()

    $World/CameraPos/Camera/Weapon.add_child(load(g.player.weapon.model).instance())


func _on_Spells_pressed() -> void:
    $UI/SpellBook.refresh()
    $UI/SpellBook.show()


func _on_MapBack() -> void:
    pass # Replace with function body.


func _on_Map_toggled(press: bool) -> void:
    $UI/VC.visible = press


func _on_MonsterInfoClose_pressed() -> void:
    get_tree().paused = false
    $UI/MonsterInfo.hide()

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        # For android
        $UI/MenuPopup.show()

func _on_HideStats_pressed() -> void:
    $UI/Stats.hide()

func _on_Stats_pressed() -> void:
    var s = $UI/Stats
    s.show_modal()
    $UI/Stats/Stats/HPMana/HP.text = str(g.player.hp) + "/" + str(g.player.max_hp)
    $UI/Stats/Stats/HPMana/MP.text = str(g.player.mana) + "/" + str(g.player.max_mana)
    $UI/Stats/Stats/Power/Power.text = str(g.player.power)
    if g.player.weapon.damage.size() > 0:
        $UI/Stats/Stats/Power/BonusLbl.show()
    else:
        $UI/Stats/Stats/Power/BonusLbl.hide()
    $UI/Stats/Stats/Power/Bonus.set_data(g.player.weapon.damage)
    $UI/Stats/Stats/Crit/Crit.text = str(g.player.crit)
    $UI/Stats/Stats/Crit/CritChance.text = str(g.player.crit_chance)
    $UI/Stats/Stats/Defence/Def.set_data({
            "normal": g.player.defence,
            "fire": g.player.defence_fire,
            "water": g.player.defence_water,
            "poison": g.player.defence_poison,
            "zap": g.player.defence_zap
       })




func _on_Settings_pressed() -> void:
    $UI/MenuPopup.show_modal(true)


func _on_DeeperMSG_pressed() -> void:
    $UI/GoDeeperMessage.show_modal()
