extends Node2D

var touched = false
var line : Line2D
const MIN_LENGTH = 30
var aprocsimated = []
var last_pos = null
const line_fx = preload("res://FX/LineFX.tscn")
var ready = false
export (NodePath) var weapon_node
signal casted(spell, success)
var weapon : Spatial
var game
var symbols = {
    "cheat": [[0,90,180,-90,0,90,180,-90,0,90,180,-90,45,-135]],
    "stun": [[0,45,90,135,180,-135 ,-90,-45,0,45,90],
             [0,45,90,135,180,-135 ,-90,-45,0,90],
            [0 ,45,90,135,180,-135,-90,-45,45,90],
            [0,45,90,135,180,-90,-45,90],
            [0,90,135,180,-135,-90,-45,0,135,90]
            ],
    "big_fireball": [[-90, -45, -135, 135], [-90,-45,-90,-135, 135], [-90,-45,-90,-135, 180, 135]],
    "fireball": [[-90, -45, -135], [-90,-45,-90,-135]],

    "big_zap" : [[-45,-135,-45, -135],
                 [-45,180, -45,-135],],
    "small_zap" :     [[-45,-135,-45],
                 [-45,180,-45],],
    "big_waterball": [[0, -45, -90,-135,180, -90],
                      [0, -45, -90,-135, -90],
                      [-45,-90,-135,180, -90]],
    "waterball": [[0, -45, -90,-135,180], [0, -45, -90,-135], [-45, -90,-135,180]],
    "big_poison_dart": [[-90,45,-90, 180], [-90,45, 90,-90, 180], [-90,45,-90, -135, 180]],
    "poison_dart": [[-90,45,-90], [-90,45, 90,-90]],
    "shield_water": [[45,90,135], [45,135]],
    "shield_fire": [[135,90,45], [135,45]],
    "shield_zap": [[90, -45], [90,-90,-45]],
    "shield_poison": [[90, 45]],
    "shield": [[90]],
    "expertise": [[-45, 45], [-45,0,45]]
   }

func _ready() -> void:
    weapon = get_node(weapon_node)



func move_staff():
    if weapon and game and game.staff_tween:
        game.staff_tween.stop_all()
        var m = get_global_mouse_position()

        m -= get_viewport_rect().size/2
        m /= get_viewport_rect().size.y

        m *= 16
#                weapon.get_parent().get_node("Target").translation = Vector3(m.x, -m.y, -10)

        weapon.rotation.y = -PI*1.5 + Vector2(0,0).angle_to_point(Vector2(m.x, 10))
        weapon.rotation.x = -PI*1.5 + Vector2(0,0).angle_to_point(Vector2(m.y, 10))

        if weapon.rotation_degrees.x < -180:
            weapon.rotation_degrees.x += 360
        weapon.rotation_degrees.x = clamp(weapon.rotation_degrees.x, 0 , 26)

func _input(event: InputEvent) -> void:
    if not ready:
        return
    if event is InputEventMouseButton or event is InputEventScreenTouch:
        touched = event.pressed
        if touched:
#            $Sparckles.show()
            $Sparckles/Particles.emitting = true
            $Sparckles.position =  get_global_mouse_position()
            aprocsimated = []
            if line and is_instance_valid(line):
                line.queue_free()
                line = null
            line = line_fx.instance()

            line.clear_points()
            add_child(line)
        elif is_instance_valid(line):

#            $Sparckles.hide()
            $Sparckles/Particles.emitting = false
            aprocsimate()

            line.queue_free()
            line = null
            game.return_staff()

    if event is InputEventMouseMotion or event is InputEventScreenDrag:
        if touched:
            var m = get_global_mouse_position()
            move_staff()
            $Sparckles.position = m
            if not last_pos or m.distance_to(last_pos) > 10 and line and is_instance_valid(line):
                line.add_point(m)
                last_pos = m
func end():
    $Sparckles/Particles.emitting = false
    if line and is_instance_valid(line):
        line.queue_free()


func add_helper(pos):
    if not has_node("helper"):
        var sp = Node2D.new()
        sp.name = "helper"
        add_child(sp)
    var ins = Sprite.new()
    ins.texture = load("res://UI/skill_not_active.png")
    get_node("helper").add_child(ins)
    ins.position = pos


func aprocsimate():
    if has_node("helper"):
        for c in get_node("helper").get_children():
            c.queue_free()

    if line and is_instance_valid(line):
        var arr = []
        var arr_points = []
        var angle = null
#        var first

        var length = 0
        var min_len = 10000

        var prev_angle

        for i in line.points.size():

            var p = line.points[i]
            if i == 0:
#                first = p
                continue

            var prev = line.points[i-1]

            var t_angle = rad2deg((p-prev).angle())
#            t_angle = int(round(t_angle/45)*45)

#                length += p.distance_to(prev)
            if i == line.points.size() - 1 and arr.size() > 0:
                arr.append({"point1": prev, "point2": p})

            elif angle != null and (abs(angle-t_angle) < 22.5 or abs(angle-t_angle) > (360-22.5)):
                arr[arr.size()-1].point2 = p
            else:
                angle = int(round(t_angle/45)*45)
                if angle == -0:
                    angle = 0
                elif angle == -180:
                    angle = 180
                arr.append({"point1": prev, "point2": p})
                length = 0


        # Check if cast valid

        var maximum = 0
        # get_max and correct angles
        if arr.size() == 0:
            return


        arr = simplify(arr, 0.1)

        var res = get_symbol(arr)
        if res:
            print("Base casted")
            emit_signal("casted", res, 1)
        else:
            arr = simplify(arr, 0.3)
            res = get_symbol(arr)
            if res:
                print("Simple casted")
                emit_signal("casted", res, 1)

        for p in arr:
            print("MAGIC: ", p.angle, ",")
        if not res:
            print("No Cast")
        print("----")

func get_symbol(arr):
    var casted = false
    var res = null
    for s in symbols:
        var valid = false
        for j in symbols[s].size():
            if abs(symbols[s][j].size() - arr.size()) <= 1:
                for i in symbols[s][j].size():
                    if arr.size() > i and arr[i].angle == symbols[s][j][i]:
                        valid = true
                        continue
                    else:

                        valid = false
                        break
            if valid:
                res = s
                casted = true
                break
        if valid:
            break

    return res
#        for a in arr:
#            add_helper(a.point1)
#            add_helper(a.point2)
#        print("Angles ")


func simplify(arr, mod=0.1):
    for a in arr:
        a.length = a.point1.distance_to(a.point2)
    var maximum = 0
    for a in arr:
        if a.length > maximum:
            maximum = a.length

    for a in arr:
        a.length = stepify(a.length/maximum, 0.1)

    var prev
    var new_arr = []

    for a in arr:
        a.angle = int(round(rad2deg((a.point2-a.point1).angle())/45)*45)
        if a.angle == -0:
            a.angle = 0
        elif a.angle == -180:
            a.angle = 180

        if prev and (prev.angle == a.angle or prev.length <= mod):
            a.length += prev.length
            a.point1 = prev.point1
            new_arr.erase(prev)


        prev = a
        new_arr.append(a)


    if new_arr[new_arr.size()-1].length <= mod:
        new_arr[new_arr.size()-2].length += new_arr[new_arr.size()-1].length
        new_arr[new_arr.size()-2].point2 = new_arr[new_arr.size()-1].point2

        new_arr.remove(new_arr.size()-1)
#        emit_signal("casted", "fireball", 1)
    return new_arr

