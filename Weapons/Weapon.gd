extends Spatial
class_name Weapon
export (NodePath) var fire_point_node
var fire_point

func _ready() -> void:
    fire_point = get_node(fire_point_node)
