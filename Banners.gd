extends Node

var admob = null
var isReal = true
var isTop = false
var adBannerId = "ca-app-pub-6126647077019498/1596814286" # [Replace with your Ad Unit ID and delete this message.]
var adInterstitialId = "ca-app-pub-6126647077019498/2096287986" # [Replace with your Ad Unit ID and delete this message.]
var adRewardedId = "ca-app-pub-6126647077019498/1952037503" # [There is no testing option for rewarded videos, so you can use this id for testing]
signal adv_closed
signal video_closed(i)
signal ad_ready(height)
var height= 0
var ready = false
func _ready():
    if(Engine.has_singleton("AdMob")):
        admob = Engine.get_singleton("AdMob")
        admob.initWithContentRating(isReal, get_instance_id(), true, "G")

#        admob.init(isReal, get_instance_id())
        loadBanner()
        loadInterstitial()
        ready = true
#        loadRewardedVideo()

#        show_banner(true)

    get_tree().connect("screen_resized", self, "onResize")



func loadBanner():
    if admob != null:
        admob.loadBanner(adBannerId, isTop)

func loadInterstitial():
    if admob != null:
        admob.loadInterstitial(adInterstitialId)

func loadRewardedVideo():
    if admob != null:
        admob.loadRewardedVideo(adRewardedId)

# Events

func show_banner(pressed):
    if not pressed:
        height = 0
        emit_signal("ad_ready", 0)
    if admob != null:
        if pressed:
            admob.showBanner()
            height = admob.getBannerHeight()
        else:
            admob.hideBanner()

func show_inter():
    if admob != null:
        admob.showInterstitial()

func show_video():
    if admob != null:
        admob.showRewardedVideo()

func _on_admob_network_error():
    print("Network Error")

func _on_admob_ad_loaded():
    print("Ad loaded success")
    emit_signal("ad_ready", admob.getBannerHeight())

func _on_interstitial_not_loaded():
    print("Error: Interstitial not loaded")

func _on_interstitial_loaded():
    print("Interstitial loaded")

func _on_interstitial_close():
    print("Interstitial closed")
    emit_signal("adv_closed")

func _on_rewarded_video_ad_loaded():
    print("Rewarded loaded success")


func _on_rewarded_video_ad_closed():
    print("Rewarded closed")
    get_node("CanvasLayer/BtnRewardedVideo").set_disabled(true)
    loadRewardedVideo()

func _on_rewarded(currency, amount):
    print("Reward: " + currency + ", " + str(amount))
    emit_signal("video_closed", amount)


# Resize

func onResize():
    if admob != null:
        admob.resize()

