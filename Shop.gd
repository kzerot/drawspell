extends Control

export (PackedScene) var item_instance
export (String) var type = "potions"
var player : Player
export var countable = true
var mode = "buy"
var shop = null
signal buyed
signal selled
var sound = null
var sep = preload("res://UI/Sep.tscn")
func init(pl, _shop=null):
    player = pl
    shop = _shop
    $Gold.text = str(player.gold)

func add_sep():
    var s = sep.instance()
    $Scroll/Cont.add_child(s)

func refresh():
    for c in $Scroll/Cont.get_children():
        c.queue_free()
    $Gold.text = str(player.gold)
    add_sep()
    if type in data:
        for p in data.get(type):
            var item = item_instance.instance()
            $Scroll/Cont.add_child(item)
            add_sep()
            item.connect("buy", self, "buy", [p])
            if type in ["potions"]:
                var item_data = data.get(type)[p]
                var player_item = player.get_item(type, p)
                var count = 0
                if item_data:
                    if not player_item:
                        player.potions.append({"name": p, "count": 0})
                        player_item = player.get_item(type, p)
                    count = player_item.count
                    item.set_item(p, item_data.descr, item_data.image, item_data.cost, player_item.count)
    elif  type in "items":
        if mode == "buy":
            for i in shop.items:
                var item = item_instance.instance()
                $Scroll/Cont.add_child(item)
                add_sep()
                item.set_item(i.name, i.descr, i.image, i.cost)
                item.connect("buy", self, "buy", [i])
        elif mode == "sell":
            for i in player.inventory:
                var item = item_instance.instance()
                $Scroll/Cont.add_child(item)
                add_sep()
                item.set_item(i.name, i.descr, i.image, int(i.cost/2))
                item.connect("buy", self, "sell", [i])

func sell(item_node, item_data):
    if player.weapon_count() >=2:
        player.gold += int(item_data.cost/2)

        shop.items.append(item_data)
        player.inventory.erase(item_data)
        refresh()
    emit_signal("selled")
    $Gold.text = str(player.gold)


func buy(item_node, item_name):
    var item_data
    if typeof(item_name) == TYPE_STRING:
        item_data = data.get(type)[item_name]
    else:
        item_data = item_name
        item_name = item_data.name

    if player.gold >= item_data.cost and (countable or player.inventory.size() < player.inventory_count):
        player.gold -= item_data.cost
        if countable:
            if not player.get_item(type, item_name):
                player.get[type].append({"name": item_name, "count": 0})
            player.change_item_count(type, item_name, 1)
            item_node.commit()
        else:
            # TODO Weapon loogic
            player.inventory.append(item_data)
            shop.items.erase(item_data)
            refresh()

    emit_signal("buyed")
    $Gold.text = str(player.gold)

func _on_Back_pressed() -> void:
    hide()


func _on_Buy_pressed() -> void:
    mode = "buy"
    refresh()


func _on_Sell_pressed() -> void:
    mode = "sell"
    refresh()
