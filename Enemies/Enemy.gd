extends Spatial
class_name Enemy


export (float) var HP = 50
#export (float) var strength = 10
#export (String) var damage_type = "normal"

export (float) var attack_freq = 2

export (String) var idle = "Idle"
export (String) var attack = "AttackBase"
export (String) var die = "Die"
export (String) var take_damage = "TakeDamage"

export (int) var experience = 3
export (int) var gold = 1

export (String) var monster_name = "Monster"
export var damage = 10
export var damage_water = 0
export var damage_fire = 0
export var damage_poison = 0
export var damage_zap = 0


export var defence = 0
export var defence_water = 0
export var defence_fire = 0
export var defence_poison = 0
export var defence_zap = 0

signal bite
signal health_changed(hp)
signal effect_cleared
var time_from_attack = 0
var max_hp
var in_battle = false
var effects = {}

var effect_id = 1

func get_gold():
    return gold + round(randf()*2)



var tick = 0
var stunned = false

func _process(delta: float) -> void:
    if in_battle and HP > 0:
        tick += delta
        if tick >= 1:
            tick = 0

            for ef in effects:
                var e = effects[ef]
                if e.type == "dot":
                    take_damage(e.damage, false)
                    emit_signal("health_changed", HP)

        if not $AnimationPlayer.current_animation == attack and not stunned:
            time_from_attack += delta
            if time_from_attack > attack_freq and not $AnimationPlayer.current_animation == take_damage:
                $AnimationPlayer.play(attack)
                time_from_attack = 0
    elif HP <= 0 and translation.y > -20:
        translate(Vector3(0, -delta*0.5, 0))

func clear_effect(fx, spell_id):
    if fx and fx.has_method("die"):
        fx.die()

    if spell_id in effects:
        var spell = effects[spell_id]
        if spell and spell.type == "stun":
            stunned = false
        effects.erase(spell_id)
        emit_signal("effect_cleared")

func add_effect(spell):
    var id = effect_id
    effect_id+=1
    spell = spell.duplicate()

    var fx = null
    if "model" in spell:
        fx = spell.model.instance()

    if fx:
        self.add_child(fx)
        fx.translation = self.global_transform.origin + Vector3(0,4,0)
        fx.rotation = self.rotation
        fx.fire()
    effects[id] = spell
    if spell.type == "stun":
            stunned = true
    if "duration" in spell:
        var timer = get_tree().create_timer(spell.duration)
        timer.connect("timeout", self, "clear_effect", [fx,  id])



func _ready() -> void:
    max_hp = HP

    $AnimationPlayer.set_default_blend_time(0.1)

    $AnimationPlayer.get_animation(idle).loop = true
    $AnimationPlayer.play(idle)
    $AnimationPlayer.connect("animation_finished", self, "animation_finished")
    if g.dungeon_data:
        var lvl = g.dungeon_data.level
        var mod = 1 + lvl/100.0 + (randf() - 0.5)* 0.2
        for val in ["damage", "damage_water", "damage_fire", "damage_poison",
                    "damage_zap",   "defence", "defence_water", "defence_fire",
                    "defence_poison", "defence_zap", "experience", "gold"]:
            if val in self and get(val) > 0:
                set(val, int(clamp(round(get(val)*mod), 0, 99)))

func attacking():
    emit_signal("bite")

func get_damage():
    var res = {}
    if damage:
        res.normal = get_stat("damage")
    if damage_water:
        res.water = get_stat("damage_water")
    if damage_fire:
        res.fire = get_stat("damage_fire")
    if damage_poison:
        res.poison = get_stat("damage_poison")
    if damage_zap:
        res.zap = get_stat("damage_zap")
    return res


func get_stat(stat):
    var val = get(stat)
    for s in effects:
        var sp = effects[s]
        if "stat_up" in sp:
            if stat in sp.stat_up:
                val += sp.stat_up[stat]
        if "stat_down" in sp:
            if stat in sp.stat_down:
                val -= sp.stat_down[stat]
    return val

func take_damage(dmg_array, need_anim = true):
    var total = 0
    var res = []
    for d in dmg_array:
        var dmg = 0
        var damage_type = "normal"
        if typeof(dmg_array) == TYPE_ARRAY:
            dmg = d.dmg
            damage_type = d.type
        else:
            damage_type = d
            dmg = dmg_array[d]
        match damage_type:
            "normal":
                dmg = dmg * float(100-get_stat("defence")) / 100
            "water":
                dmg = dmg * float(100-get_stat("defence_water")) / 100
            "zap":
                dmg = dmg * float(100-get_stat("defence_zap")) / 100
            "fire":
                dmg = dmg * float(100-get_stat("defence_fire")) / 100
            "poison":
                dmg = dmg * float(100-get_stat("defence_poison")) / 100
        dmg = stepify(dmg, 0.1)
        HP -= dmg
        total += dmg
        var t = {damage_type: dmg}
        res.append(t)

    if need_anim and HP > 0 and $AnimationPlayer.has_animation(take_damage):
        if $AnimationPlayer.current_animation == attack and (randf() > 0.3 or total < max_hp / 4.0):
            print("Didn't breake'")
        elif  $AnimationPlayer.current_animation == take_damage:
            pass # Do nothing
        else:
            print("Enemy took dmg")
            $AnimationPlayer.play(take_damage)
    elif HP <=0 and $AnimationPlayer.has_animation(die):
        $AnimationPlayer.play(die)
    return {"damage": res, "total": total, "crit": false}

func animation_finished(anim: String):
    if HP > 0:
        $AnimationPlayer.play(idle)
