extends Node

func play(sound: String):
    var node : AudioStreamPlayer
    if has_node(sound):
        node = get_node(sound)
    elif has_node(sound.capitalize()):
        node = get_node(sound.capitalize())

    if node:
        node.play(0)
func stop(sound: String = "Steps"):
    var node : AudioStreamPlayer
    if has_node(sound):
        node = get_node(sound)
    elif has_node(sound.capitalize()):
        node = get_node(sound.capitalize())

    if node:
        node.stop()
