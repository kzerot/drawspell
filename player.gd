extends Object
class_name Player
var max_hp = 100
var max_mana = 100
var gold = 50
var hp = 100
var mana = 100
var experience = 0
var defence = 0
var defence_water = 0
var defence_fire = 0
var defence_poison = 0
var defence_zap = 0
var power = 10
var crit = 30
var crit_chance = 10
var effects = []
var upgrades = ["fireball_1"]
var potions = [
    {"name": "hp_small", "count": 3},
    {"name": "hp_medium", "count": 0},
    {"name": "hp_big", "count": 0},

    {"name": "mana_small", "count": 3},
    {"name": "mana_medium", "count": 0},
    {"name": "mana_big", "count": 0},

    {"name": "def_small", "count": 2},
    {"name": "def_medium", "count": 0},
    {"name": "def_big", "count": 0},

    {"name": "water_poison_small", "count": 0},
    {"name": "water_poison_medium", "count": 0},
    {"name": "water_poison_big", "count": 0},

    {"name": "fire_zap_small", "count": 0},
    {"name": "fire_zap_medium", "count": 0},
    {"name": "fire_zap_big", "count": 0},
]
var inventory = [{"type": "staff", "cost":0, "damage":{}, "name":"Rusty staff", "descr": "Start staff",
                  "image": "res://UI/staffs/staff_fire.png", "model": "res://Weapons/StaffFire.tscn"}]
var inventory_count = 6

var weapon = inventory[0]
var armor

func has_slot():
    return inventory.size() < inventory_count

func weapon_count():
    var n = 0
    for i in inventory:
        if i.type == "staff":
            n+=1
    return n

func change_item_count(type, item, i):
    for it in get(type):
        if it.name == item:
            it.count += i
            break

func get_item(type, item):
    var items = get(type)
    for i in items:
        if i.name == item:
            return i
    return false

func get_potions():
    var arr = []
    for p in potions:
        if not "data" in p:
            p.data = data.potions[p.name]
        if p.count >0:
            arr.append(p)
    return arr

func get_stat(stat):
    var val = get(stat)

    var stat_ups ={"buff":0, "pot_buff":0}
    var stat_downs ={"buff":0, "pot_buff":0}

    for sp in effects:

        if "stat_up" in sp and stat in sp.stat_up:
            stat_ups[sp.type] = max(sp.stat_up[stat], stat_ups[sp.type] )

        if "stat_down" in sp and stat in sp.stat_down:
            stat_downs[sp.type] = max(sp.stat_down[stat], stat_downs[sp.type] )


    for st in stat_ups:
        val += stat_ups[st]
    for st in stat_downs:
        val -= stat_downs[st]
    return val

func is_crit():
    return crit_chance/100.0 >= randf()

func take_damage(damage):

    var _defence = get_stat("defence")
    var res = []
    var total = 0

    for d in damage:
        var damage_type = d
        var dmg = damage[d]
        match damage_type:
            "normal":
                dmg = dmg * float(100-_defence) / 100
            "water":
                dmg = dmg * float(100- max(get_stat("defence_water"), _defence/2.5)) / 100
            "zap":
                dmg = dmg * float(100- max(get_stat("defence_zap"), _defence/2.5)) / 100
            "fire":
                dmg = dmg * float(100- max(get_stat("defence_fire"), _defence/2.5)) / 100
            "poison":
                dmg = dmg * float(100- max(get_stat("defence_poison"), _defence/2.5)) / 100
        dmg = stepify(dmg, 0.1)
        var t = {damage_type: dmg}
        res.append(t)
        total += dmg
        hp -= dmg
    return {"damage": res, "total": total, "crit": false}

func pack():
    var vars = ["gold","hp","mana","experience","upgrades", "potions", "inventory", "weapon"]
    var dict = {}
    for d in vars:
        dict[d] = get(d)
    return dict

func unpack(dict):
    var vars = ["gold","hp","mana","experience","upgrades", "potions", "inventory", "weapon"]
    for d in vars:
        set(d, dict[d])
        if d == "weapon":
            for w in inventory:
                if g.compare_weapon(dict[d], w):
                    weapon = w

func equip_first_weapon():
    for w in inventory:
        if w.type == "staff":
            weapon = w
            break
func check_weapon():
    var has_weapon = false
    if not weapon:
        equip_first_weapon()
    for w in inventory:
        if g.compare_weapon(weapon, w):
            has_weapon = true
            break
    if not has_weapon:
        equip_first_weapon()
