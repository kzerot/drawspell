extends Node2D

var touches = {}
var scaling = 1

var length = 0

var game
var lab
var player = null
var camera_tween : Tween
var explored = []
func load_map(_game):
    camera_tween = Tween.new()
    add_child(camera_tween)
    game = _game
    lab = game.lab
    var treasures = game.treasures
    for f in lab.map.floors:
        get_map("floor", f.x, f.y, 0)
    for w in lab.map.walls:
        get_map("wall", w.x, w.y, w.angle, w.base_x, w.base_y)
    for f in treasures:
        get_map("chest", f.x, f.y, 0)
    get_map("down", lab.map.enter.x, lab.map.enter.y, 0)

    player = Sprite.new()
    player.name = "player"
    player.texture = load("res://2dMap/player.png")
    add_child(player)
#    for x in lab.width:
#        for y in lab.height:
#            get_map("terra_incognita", x, y, 0)
    set_boundaries(Vector2(lab.width*80, lab.height*80))
    update()

func update():

    if not Vector2(game.x,game.y) in explored:
        explored.append(Vector2(game.x,game.y))

    var dirs = [Vector2(-1,0), Vector2(1,0),Vector2(0,0),Vector2(0,-1),Vector2(0,1)]
    for dir in dirs:
        var x = game.x + dir.x
        var y = game.y + dir.y
        if x < 0 or y < 0 or x >= lab.width or y >= lab.height:
            continue
        if lab.array[x][y]:
            open_terra(x, y)
    center()
#    player.position = Vector2(game.x,game.y)*80
    player.rotation = game.get_angle()

func load_explored(expl):
    var dirs = [Vector2(-1,0), Vector2(1,0),Vector2(0,0),Vector2(0,-1),Vector2(0,1)]
    for e in expl:
        for dir in dirs:
            var x = e.x + dir.x
            var y = e.y + dir.y
            if x < 0 or y < 0 or x >= lab.width or y >= lab.height:
                continue
            if lab.array[x][y]:
                if not Vector2(x,y) in explored:
                    explored.append(Vector2(x,y))
                open_terra(x, y)

func delete_treasure(x,y):
    var n = "f_" + str(x) + "|" +str(y)
    if has_node(n):
        n = get_node(n)
        if n.has_node("treasure"):
            n.get_node("treasure").hide()

func tween_camera_to(x,y):
    var pos = Vector2(x, y) * 80
    camera_tween.interpolate_property($MapCam, "position",
                    $MapCam.position,
                    pos, 0.5,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    camera_tween.interpolate_property(player, "position",
                    player.position,
                    pos, 0.5,
                    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)

    camera_tween.start()

func open_terra(x,y):
    var terra =  "f_" + str(x) + "|" +str(y)
    if has_node(terra):
        terra = get_node(terra)
        terra.show()

func center():
    tween_camera_to( game.x, game.y)
#    $MapCam.position = Vector2(game.x, game.y) * 80

func center_hard():
    $MapCam.position = Vector2(game.x, game.y) * 80

func get_map(det,x,y,angle = 0, bx=0, by=0):
    var tex = Sprite.new()
    var img = load("res://2dMap/"+det+".png")
    tex.texture = img

    if det == "floor":
        tex.name = "f_" + str(x) + "|" +str(y)
        tex.position = Vector2(x,y) * 80
        add_child(tex)
        tex.hide()
    elif det == "wall":

        if not has_node("f_" + str(bx) + "|" +str(by)):
            print("NO NODE")
            return
        get_node("f_" + str(bx) + "|" +str(by)).add_child(tex)
        tex.position = Vector2(x,y) * 80
        tex.rotation = deg2rad(angle)
    else:
        if det == "chest":
            tex.name = "treasure"

        if not has_node("f_" + str(x) + "|" +str(y)):
            print("NO NODE")
            return
        get_node("f_" + str(x) + "|" +str(y)).add_child(tex)

    return tex

func set_boundaries(v2):
    return
#    $MapCam.limit_right = v2.x
#    $MapCam.limit_bottom = v2.y

func _ready() -> void:
    set_current(true)


func set_current(val):
    $MapCam.current = val
    $MapCam.zoom = Vector2(2,2)

func _input(event: InputEvent) -> void:
    if event is InputEventScreenTouch:
        if event.pressed:
#            var scaling = 1
            if 1 in touches and 0 in touches:
                length = touches[0].distance_to(touches[1])

            touches[event.index] = event.position
        else:
            touches.erase(event.index)

    if event is InputEventMouseButton:
        if event.button_index == BUTTON_WHEEL_UP:
            var sc = $MapCam.zoom.x
            sc = clamp(sc * (1.01), 1, 3)
            var ds = sc - scale.x
            $MapCam.zoom = Vector2(sc,sc)
            center_hard()
        elif event.button_index == BUTTON_WHEEL_DOWN:
            var sc = $MapCam.zoom.x
            sc = clamp(sc * (0.99), 1, 3)
            var ds = sc - scale.x
            $MapCam.zoom = Vector2(sc,sc)
            center_hard()
    if event is InputEventScreenDrag:
        touches[event.index] = event.position


        if 1 in touches and 0 in touches:
            var new_len = touches[0].distance_to(touches[1])
            var magn = (new_len - length) / 300
            length = new_len
            if abs(magn) > 0.1:
                return

            var sc = $MapCam.zoom.x
            sc = clamp(sc * (1-magn), 1, 3)
            var ds = sc - scale.x
            $MapCam.zoom = Vector2(sc,sc)
            center_hard()
#        elif touches.size() == 1:
#            $MapCam.position -= event.relative

