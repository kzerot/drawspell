extends Control

var player : Player
signal weapon_changed

var weapon_inst = preload("res://UI/InventoryItem.tscn")
var armor_inst = preload("res://UI/InventoryItem.tscn")

func _ready() -> void:
    init()

func init():
    player = g.player
    refresh()

func refresh():
    $Gold.text = str(player.gold)
    $Slots.text = str(player.inventory.size())+"/"+str(player.inventory_count)
    for c in $Scroll/Cont.get_children():
        $Scroll/Cont.remove_child(c)
        c.queue_free()

    for i in player.inventory:
        var item
        if i.type == "staff":
            item = weapon_inst.instance()
        else:
            item = armor_inst.instance()
        $Scroll/Cont.add_child(item)
        item.set_item(i.name, i.descr, i.image)
        item.connect("equip", self, "equip", [i])

    semi_refresh()

func equip(item_node, item_data):
    if item_data.type == "staff":
        if player.weapon != item_data:
            player.weapon = item_data
            emit_signal("weapon_changed")
    else:
        if player.armor != item_data:
            player.armor = item_data
    semi_refresh()

func semi_refresh():
    for i in player.inventory.size():
        if g.compare_weapon(player.inventory[i], player.weapon) or \
            g.compare_weapon(player.inventory[i], player.armor):
            $Scroll/Cont.get_child(i).equipped(true)
        else:
            $Scroll/Cont.get_child(i).equipped(false)
#    emit_signal("selled")
#    $Gold.text = str(player.gold)


func _on_Back_pressed() -> void:
    hide()


