extends Node
var potions = {
        "hp_small":  {
            "image": "res://UI/potions/hp_small.png", "effects": {"restore_hp": 10, "regen_hp": {"value": 8}}, "duration": 5,
            "descr": "hp_small_descr", "cost": 10
            },
        "hp_medium": {
            "image": "res://UI/potions/hp_medium.png", "effects": {"restore_hp": 20, "regen_hp": {"value": 12}}, "duration": 5,
            "descr": "hp_medium_descr", "cost": 20
        },
        "hp_big" :{
            "image": "res://UI/potions/hp_full.png", "effects": {"restore_hp": 30, "regen_hp": {"value": 20}}, "duration": 5,
            "descr": "hp_big_descr", "cost": 50
        },

        "mana_small": {
            "image": "res://UI/potions/mana_small.png", "effects": {"restore_mana": 10, "regen_mana": {"value": 6}}, "duration": 10,
            "descr": "mana_small_descr", "cost": 10
        },
        "mana_medium": {
            "image": "res://UI/potions/mana_medium.png", "effects": {"restore_mana": 20, "regen_mana": {"value": 8}}, "duration": 10,
            "descr": "mana_medium_descr", "cost": 20
        },
        "mana_big": {
            "image": "res://UI/potions/mana_full.png", "effects": {"restore_mana": 30, "regen_mana": {"value": 14}}, "duration": 10,
            "descr": "mana_big_descr", "cost": 50
        },

        "def_small": {
            "image": "res://UI/potions/stars_small.png", "effects": {"stat_up": {"defence": 20}}, "duration": 10,
            "descr": "def_small_descr", "cost": 30
        },
        "def_medium": {
            "image": "res://UI/potions/stars_medium.png", "effects":{"stat_up": {"defence": 30}}, "duration": 10,
            "descr": "def_medium_descr", "cost": 60
        },
        "def_big": {
            "image": "res://UI/potions/stars_full.png", "effects":{"stat_up": {"defence": 40}},  "duration": 10,
            "descr": "def_big_descr", "cost": 100
        },

        "water_poison_small": {
            "image": "res://UI/potions/green_small.png", "effects": {"stat_up": {"defence_water": 20, "defence_poison": 20}}, "duration": 10,
            "descr": "water_poison_small_descr", "cost": 50
        },
        "water_poison_medium": {
            "image": "res://UI/potions/green_medium.png", "effects":{"stat_up":  {"defence_water": 30, "defence_poison": 30}}, "duration": 10,
            "descr": "water_poison_medium_descr", "cost": 100
        },
        "water_poison_big": {
            "image": "res://UI/potions/green_full.png", "effects":{"stat_up":  {"defence_water": 40, "defence_poison": 40}}, "duration": 10,
            "descr": "water_poison_big_descr", "cost": 150
        },

        "fire_zap_small": {
            "image": "res://UI/potions/yellow_small.png", "effects": {"stat_up": {"defence_fire": 20, "defence_zap": 20}}, "duration": 10,
            "descr": "fire_zap_small_descr", "cost": 50
        },
        "fire_zap_medium": {
            "image": "res://UI/potions/yellow_medium.png", "effects":{"stat_up": {"defence_fire": 30, "defence_zap":30}}, "duration": 10,
            "descr": "fire_zap_medium_descr", "cost": 100
        },
        "fire_zap_big": {
            "image": "res://UI/potions/yellow_full.png", "effects":{"stat_up": {"defence_fire": 40, "defence_zap": 40}}, "duration": 10,
            "descr": "fire_zap_big_descr", "cost": 150
        },
    }

func get_monster(m_name):
    for m in monsters:
        if m.name == m_name:
            return m

var monsters = [
    {   "name": "common_spider",
        "model": preload("res://Enemies/Spider/Spider.tscn"),
        "min_level": 0,
        "max_level": 20
    },
    {   "name": "poison_spider",
        "model": preload("res://Enemies/Spider/SpiderPoison.tscn"),
        "min_level": 3,
        "max_level": 33
    },
    {   "name": "water_spider",
        "model": preload("res://Enemies/Spider/SpiderWater.tscn"),
        "min_level": 1,
        "max_level": 30
    },
    {   "name": "bug",
        "model": preload("res://Enemies/Bug/Bug.tscn"),
        "min_level": 5,
        "max_level": 50
    },
    {   "name": "crystal_bug",
        "model": preload("res://Enemies/Bug/CrystalBug.tscn"),
        "min_level": 7,
        "max_level": 65
    },
    {   "name": "blade_bug",
        "model": preload("res://Enemies/Bug/CrystalBug.tscn"),
        "min_level": 9,
        "max_level": 80
    },
    {   "name": "blade_crystal_bug",
        "model": preload("res://Enemies/Bug/BladeCrystalBug.tscn"),
        "min_level": 14,
        "max_level": 100
    },
    {   "name": "fire_worm",
        "model": preload("res://Enemies/Worm/WormFire.tscn"),
        "min_level": 18,
        "max_level": 100
    },
    {   "name": "poison_worm",
        "model": preload("res://Enemies/Worm/WormPoison.tscn"),
        "min_level": 19,
        "max_level": 100
    },
    {   "name": "water_worm",
        "model": preload("res://Enemies/Worm/WormWater.tscn"),
        "min_level": 20,
        "max_level": 100
    },

    {   "name": "scorpio",
        "model": preload("res://Enemies/Scorpio/Scorpio.tscn"),
        "min_level": 20,
        "max_level": 75
    },
    {   "name": "grand_scorpio",
        "model": preload("res://Enemies/Scorpio/GrandScorpio.tscn"),
        "min_level": 25,
        "max_level": 100
    },
    {   "name": "golem",
        "model": preload("res://Enemies/Golem/Golem.tscn"),
        "min_level": 50,
        "max_level": 100
    },
]

var treasures = {
        0: {
                "name": "simple",
                "gold": 10,
                "gold_variant": 5,
                "potions" : ["hp_small", "mana_small"],
                "potions_count": 2
           },
        1: {
                "name": "fair",
                "gold": 20,
                "gold_variant": 5,
                "potions" : ["hp_small", "mana_small"],
                "potions_count": 3,
                "item_chance": 0.1
           },
        2: {
                "name": "good",
                "gold": 30,
                "gold_variant": 10,
                "potions" : ["hp_small", "mana_small"],
                "potions_count": 4,
                "item_chance": 0.2
           },
        3: {
                "name": "big",
                "gold": 40,
                "gold_variant": 15,
                "potions" : ["hp_small", "mana_small"],
                "potions_count": 5,
                "item_chance": 0.4
           },
        4: {
                "name": "great",
                "gold": 50,
                "gold_variant": 20,
                "potions" : ["hp_small", "mana_small"],
                "potions_count": 2,
                "item_chance": 1.0
           }
   }

