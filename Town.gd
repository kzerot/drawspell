extends Control

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var shop_weapon
func save():
    g.save("town")

func greet_ok():
    g.first_time.town = false
    save()

func _ready() -> void:
    if g.first_time.town:
        var greet = load("res://UI/Greeting.tscn").instance()
        add_child(greet)
        greet.connect("closed", self, "greet_ok")


    save()
    $LevelUp.init(g.player)
    $Potions.use_enabled = false
    $Shop.init(g.player)
    $Shop.connect("buyed", self, "refresh", ["coin"])
    $ShopItems.connect("buyed", self, "refresh", ["coin"])
    $ShopItems.connect("selled", self, "refresh", ["coin"])
    $LevelUp.connect("recalc_player", self, "refresh")
    shop_weapon = ShopGenerator.new()
    shop_weapon.init(g.max_level)
    $ShopItems.init(g.player, shop_weapon)
    $Inventory.init()

    g.player.hp  = g.player.max_hp
    g.player.mana = g.player.max_mana
    refresh()

    var b = g.banners

    if not g.show_banners:
        adjust_banner(0)
        b.show_banner(false)
    elif b and b.ready:
        b.connect("ad_ready", self, "adjust_banner")
        b.show_banner(true)
        adjust_banner(b.admob.getBannerHeight())

    g.wait_and_execute(0.5, self, "hide_loading")

func hide_loading():
    $Loading.hide()

func adjust_banner(h):
    self.margin_bottom = -h
#    $Black.rect_position.y = get_viewport().get_size_override().y - h
#    print("MAGIC: ", $Black.rect_position.y )


func refresh(sound=null):
    if sound:
        $Sounds.play(sound)
    g.player.check_weapon()
    $Coins.text = str(g.player.gold)
    $Exp.text = "Exp " + str(g.player.experience)
    g.save("town")



func _on_BuyPotions_pressed() -> void:
    $Shop.show()
    $Shop.refresh()

func _notification(what):
       if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        # For android
            $MenuPopup.show()

func _on_BuyWeapon_pressed() -> void:
#    $ShopItems/Gold.text \
    $ShopItems.show()
    $ShopItems.refresh()


func _on_Skills_pressed() -> void:
    $LevelUp.show()


func _on_Potions_pressed() -> void:
    $Potions.fill_slots(g.player.get_potions())
    $Potions.show()


func _on_Potions_item_used(item) -> void:
    $Sounds.play("Bottle")
    g.use_potion(item)
    refresh()
    save()


func _on_LevelUp_recalc_player() -> void:
    g.recalc_player()
    save()


func _on_Dungeon_pressed() -> void:
    $ToDungeon/Cont/CounterCont/Labels/Level.text = str(g.max_level)
    $ToDungeon.show_modal()


func _on_Up_pressed() -> void:
    var current = int($ToDungeon/Cont/CounterCont/Labels/Level.text)
    if current - 1 >= 0:
        $ToDungeon/Cont/CounterCont/Labels/Level.text = str(current-1)

func _on_Down_button_down() -> void:
    var current = int($ToDungeon/Cont/CounterCont/Labels/Level.text)
    if current + 1 <= g.max_level:
        $ToDungeon/Cont/CounterCont/Labels/Level.text = str(current+1)

func _on_Go_pressed() -> void:
    $ToDungeon.hide()
    $Loading.show()
    g.wait_and_execute(0.2, g, "dungeon", [int($ToDungeon/Cont/CounterCont/Labels/Level.text)])
#    g.dungeon(int($ToDungeon/Cont/CounterCont/Labels/Level.text))


func _on_Inventory_pressed() -> void:
    $Inventory.show()
    $Inventory.refresh()


func _on_Settings_pressed() -> void:
    $MenuPopup.show_modal(true)


func _on_MenuPopup_back() -> void:
    $MenuPopup.hide()
