extends Node

const skill_order = [
    "expertise",
    "fireball", "big_fireball",
    "waterball", "big_waterball",
    "poison_dart", "big_poison_dart",
    "small_zap", "big_zap",
    "stun",
    "shield", "shield_fire", "shield_water", "shield_poison", "shield_zap"
    ]

const skills = {
    "fireball_1":
        {
            "add_spell": {
                "model":preload("res://FX/Fireball.tscn"), "type":"missile", "mana": 15,
                "damage": {"fire": 5}
            },
            "name": "fireball"
        },
    "fireball_2":
        {
            "add_spell": {
                "model":preload("res://FX/Fireball.tscn"), "type":"missile", "mana": 15,
                "damage": {"fire": 6}
            },
            "replace": true,
            "name": "fireball"
        },
    "fireball_3":
        {
            "add_spell": {
                "model":preload("res://FX/Fireball.tscn"), "type":"missile", "mana": 15,
                "damage": {"fire": 7}
            },
            "replace": true,
            "name": "fireball"
        },
    "fireball_4":
        {
            "add_spell": {
                "model":preload("res://FX/BigFireball.tscn"), "type":"missile", "mana": 25,
                "damage": {"fire": 10},
                "effect": {"type": "dot", "damage": {"fire": 1}, "duration": 10}
            },
            "name": "big_fireball"
        },
    "fireball_5":
        {
            "add_spell": {
                "model":preload("res://FX/BigFireball.tscn"), "type":"missile", "mana": 25,
                "damage": {"fire": 12},
                "effect": {"type": "dot", "damage": {"fire": 1.5}, "duration": 10}
            },
            "replace": true,
            "name": "big_fireball"
        },
    "fireball_6":
        {
            "add_spell": {
                "model":preload("res://FX/BigFireball.tscn"), "type":"missile", "mana": 25,
                "damage": {"fire": 15},
                "effect": {"type": "dot", "damage": {"fire": 2}, "duration": 10}
            },
            "replace": true,
            "name": "big_fireball"
        },
    "waterball_1":
        {
            "add_spell": {
                "model":preload("res://FX/Waterball.tscn"), "type":"missile", "mana": 15,
                "damage": {"water": 5}
            },
            "name": "waterball"
        },
    "waterball_2":
        {
            "add_spell": {
                "model":preload("res://FX/Waterball.tscn"), "type":"missile", "mana": 15,
                "damage": {"water": 6}
            },
            "name": "waterball",
            "replace": true
        },
    "waterball_3":
        {
            "add_spell": {
                "model":preload("res://FX/Waterball.tscn"), "type":"missile", "mana": 15,
                "damage": {"water": 7}
            },
            "name": "waterball",
            "replace": true
        },
    "waterball_4":
        {
            "add_spell": {
                "model":preload("res://FX/BigWaterball.tscn"), "type":"missile", "mana": 25,
                "damage": {"water": 12}
            },
            "name": "big_waterball"
        },
    "waterball_5":
        {
            "add_spell": {
                "model":preload("res://FX/BigWaterball.tscn"), "type":"missile", "mana": 25,
                "damage": {"water": 15}
            },
            "name": "big_waterball",
            "replace": true
        },
    "waterball_6":
        {
            "add_spell": {
                "model":preload("res://FX/BigWaterball.tscn"), "type":"missile", "mana": 25,
                "damage": {"water": 17}
            },
            "name": "big_waterball",
            "replace": true
        },
    "zap_1":
        {
            "add_spell": {
                "model":preload("res://FX/Zap.tscn"), "type":"missile", "mana": 15,
                "damage": {"zap": 5}
            },
            "name": "small_zap"
        },
    "zap_2":
        {
            "add_spell": {
                "model":preload("res://FX/Zap.tscn"), "type":"missile", "mana": 15,
                "damage": {"zap": 6}
            },
            "name": "small_zap",
            "replace": true
        },
    "zap_3":
        {
            "add_spell": {
                "model":preload("res://FX/Zap.tscn"), "type":"missile", "mana": 15,
                "damage": {"zap": 7}
            },
            "name": "small_zap",
            "replace": true
        },
    "zap_4":
        {
            "add_spell": {
                "model":preload("res://FX/BigZap.tscn"), "type":"missile", "mana": 25,
                "damage": {"zap": 10}, "effect": {"type": "stun", "duration": 0.5}
            },
            "name": "big_zap"
        },
    "zap_5":
        {
            "add_spell": {
                "model":preload("res://FX/BigZap.tscn"), "type":"missile", "mana": 25,
                "damage": {"zap": 12}, "effect": {"type": "stun", "duration": 1}
            },
            "name": "big_zap",
            "replace": true
        },
    "zap_6":
        {
            "add_spell": {
                "model":preload("res://FX/BigZap.tscn"), "type":"missile", "mana": 25,
                "damage": {"zap": 15}, "effect": {"type": "stun", "duration": 1.5}
            },
            "name": "big_zap",
            "replace": true
        },
    "poison_dart_1":
        {
            "add_spell": {
                "model":preload("res://FX/PoisonDart.tscn"), "type":"missile", "mana": 15,
                "damage": {"poison": 5}
            },
            "name": "poison_dart"
        },
    "poison_dart_2":
        {
            "add_spell": {
                "model":preload("res://FX/PoisonDart.tscn"), "type":"missile", "mana": 15,
                "damage": {"poison": 6}
            },
            "name": "poison_dart",
            "replace": true
        },
    "poison_dart_3":
        {
            "add_spell": {
                "model":preload("res://FX/PoisonDart.tscn"), "type":"missile", "mana": 15,
                "damage": {"poison": 7}
            },
            "name": "poison_dart",
            "replace": true
        },
    "poison_dart_4":
        {
            "add_spell": {
                "model":preload("res://FX/BigPoisonDart.tscn"), "type":"missile", "mana": 25,
                "damage": {"poison": 8},
                "effect": {"type": "dot", "damage": {"poison": 3}, "duration": 6}
            },
            "name": "big_poison_dart"
        },
    "poison_dart_5":
        {
            "add_spell": {
                "model":preload("res://FX/BigPoisonDart.tscn"), "type":"missile", "mana": 25,
                "damage": {"poison": 9},
                "effect": {"type": "dot", "damage": {"poison": 4}, "duration": 7}
            },
            "name": "big_poison_dart",
            "replace": true
        },
    "poison_dart_6":
        {
            "add_spell": {
                "model":preload("res://FX/BigPoisonDart.tscn"), "type":"missile", "mana": 25,
                "damage": {"poison": 10},
                "effect": {"type": "dot", "damage": {"poison": 5}, "duration": 8}
            },
            "name": "big_poison_dart",
            "replace": true
        },
    "power_1":
        {
            "stat_up": {"stat": "power", "value": 5},
            "name": "power"
        },
    "power_2":
        {
            "stat_up": {"stat": "power", "value": 5},
            "name": "power"
        },
    "shield_1":
        {
            "add_spell": {"model":preload("res://FX/Shield.tscn"), "type":"buff", "mana": 5, "duration": 2,
                         "stat_up": {"defence": 40,
                                     "defence_water": 10,
                                    "defence_zap": 10,
                                    "defence_fire": 10,
                                    "defence_poison": 10}},
            "name": "shield"
        },
    "shield_2":
        {
            "add_spell": {"model":preload("res://FX/Shield.tscn"), "type":"buff", "mana": 5, "duration": 4,
                         "stat_up": {"defence": 60,
                                     "defence_water": 20,
                                    "defence_zap": 20,
                                    "defence_fire": 20,
                                    "defence_poison": 20}},
            "name": "shield",
            "replace": true
        },
    "shield_water_1":
        {
            "add_spell": {"model":preload("res://FX/ShieldWater.tscn"), "type":"buff", "mana": 5, "duration": 2, "stat_up": {"defence_water": 40}},
            "name": "shield_water"
        },
    "shield_water_2":
        {
            "add_spell": {"model":preload("res://FX/ShieldWater.tscn"), "type":"buff", "mana": 5, "duration": 3, "stat_up": {"defence_water": 50}},
            "name": "shield_water",
            "replace": true
        },
    "shield_water_3":
        {
            "add_spell": {"model":preload("res://FX/ShieldWater.tscn"), "type":"buff", "mana": 5, "duration": 4, "stat_up": {"defence_water": 60}},
            "name": "shield_water",
            "replace": true
        },
    "shield_zap_1":
        {
            "add_spell": {"model":preload("res://FX/ShieldZap.tscn"), "type":"buff", "mana": 5, "duration": 2, "stat_up": {"defence_zap": 40}},
            "name": "shield_zap"
        },
    "shield_zap_2":
        {
            "add_spell": {"model":preload("res://FX/ShieldZap.tscn"), "type":"buff", "mana": 5, "duration": 3, "stat_up": {"defence_zap": 50}},
            "name": "shield_zap",
            "replace": true
        },
    "shield_zap_3":
        {
            "add_spell": {"model":preload("res://FX/ShieldZap.tscn"), "type":"buff", "mana": 5, "duration": 4, "stat_up": {"defence_zap": 60}},
            "name": "shield_zap",
            "replace": true
        },
    "shield_fire_1":
        {
            "add_spell": {"model":preload("res://FX/ShieldFire.tscn"), "type":"buff", "mana": 5, "duration": 2, "stat_up": {"defence_fire": 40}},
            "name": "shield_fire"
        },
    "shield_fire_2":
        {
            "add_spell": {"model":preload("res://FX/ShieldFire.tscn"), "type":"buff", "mana": 5, "duration": 3, "stat_up": {"defence_fire": 50}},
            "name": "shield_fire",
            "replace": true
        },
    "shield_fire_3":
        {
            "add_spell": {"model":preload("res://FX/ShieldFire.tscn"), "type":"buff", "mana": 5, "duration": 4, "stat_up": {"defence_fire": 60}},
            "name": "shield_fire",
            "replace": true
        },
    "shield_poison_1":
        {
            "add_spell": {"model":preload("res://FX/ShieldPoison.tscn"), "type":"buff", "mana": 5, "duration": 2, "stat_up": {"defence_poison": 40}},
            "name": "shield_poison"
        },
    "shield_poison_2":
        {
            "add_spell": {"model":preload("res://FX/ShieldPoison.tscn"), "type":"buff", "mana": 5, "duration": 3, "stat_up": {"defence_poison": 50}},
            "name": "shield_poison",
            "replace": true
        },
    "shield_poison_3":
        {
            "add_spell": {"model":preload("res://FX/ShieldPoison.tscn"), "type":"buff", "mana": 5, "duration": 4, "stat_up": {"defence_poison": 60}},
            "name": "shield_poison",
            "replace": true
        },
    "power_3":
        {
            "stat_up": {"stat": "power", "value": 5},
            "name": "power"
        },
    "defence_1":
        {
            "stat_up": {"stat": "defence", "value": 5},
            "name": "defence"
        },
    "defence_2":
        {
            "stat_up": {"stat": "defence", "value": 5},
            "name": "defence"
        },
    "defence_3":
        {
            "stat_up": {"stat": "defence", "value": 5},
            "name": "defence"
        },
    "defence_water_1":
        {
            "stat_up": {"stat": "defence_water", "value": 10},
            "name": "defence_water"
        },
    "defence_zap_1":
        {
            "stat_up": {"stat": "defence_zap", "value": 10},
            "name": "defence_zap"
        },
    "defence_poison_1":
        {
            "stat_up": {"stat": "defence_poison", "value": 10},
            "name": "defence_poison"
        },
    "defence_fire_1":
        {
            "stat_up": {"stat": "defence_fire", "value": 10},
            "name": "defence_fire"
        },
    "stun_1":
        {
            "add_spell": {
                "model":preload("res://FX/Stun.tscn"), "type":"missile", "mana": 20, "effect": {"type": "stun", "duration": 2},
                "damage": {"zap": 5}
            },
            "name": "stun"
        },
    "stun_2":
        {
            "add_spell": {
                "model":preload("res://FX/Stun.tscn"), "type":"missile", "mana": 20,  "effect": {"type": "stun", "duration": 3},
                "damage": {"zap": 5}
            },
            "name": "stun",
            "replace": true
        },
    "expertise_1":
        {
            "add_spell": {
                "type":"expertise", "mana": 5
            },
            "name": "expertise"
        },
    "crit_1":
        {
            "stat_up": {"stat": "crit", "value": 5},
            "name": "crit"
        },
    "power_4":
        {
            "stat_up": {"stat": "power", "value": 5},
            "name": "power"
        },
    "crit_chance_1":
        {
            "stat_up": {"stat": "crit_chance", "value": 10},
            "name": "crit_chance"
        },
    "crit_chance_2":
        {
            "stat_up": {"stat": "crit_chance", "value": 10},
            "name": "crit_chance"
        },
    "crit_chance_3":
        {
            "stat_up": {"stat": "crit_chance", "value": 10},
            "name": "crit_chance"
        },
    "crit_2":
        {
            "stat_up": {"stat": "crit", "value": 10},
            "name": "crit"
        },
    "crit_3":
        {
            "stat_up": {"stat": "crit", "value": 15},
            "name": "crit"
        },
    "crit_4":
        {
            "stat_up": {"stat": "crit", "value": 20},
            "name": "crit"
        },
    "hp_up_1":
        {
            "stat_up": {"stat": "max_hp", "value": 10},
            "name": "hp_up"
        },
    "hp_up_2":
        {
            "stat_up": {"stat": "max_hp", "value": 10},
            "name": "hp_up"
        },
    "hp_up_3":
        {
            "stat_up": {"stat": "max_hp", "value": 10},
            "name": "hp_up"
        },
    "hp_up_4":
        {
            "stat_up": {"stat": "max_hp", "value": 10},
            "name": "hp_up"
        },
    "hp_up_5":
        {
            "stat_up": {"stat": "max_hp", "value": 10},
            "name": "hp_up"
        },
    "mana_up_s_1":
        {
            "stat_up": {"stat": "max_mana", "value": 10},
            "name": "mana_up"
        },
    "mana_up_s_2":
        {
            "stat_up": {"stat": "max_mana", "value": 10},
            "name": "mana_up"
        },
    "mana_up_s_3":
        {
            "stat_up": {"stat": "max_mana", "value": 10},
            "name": "mana_up"
        },
    "mana_up_1":
        {
            "stat_up": {"stat": "max_mana", "value": 10},
            "name": "mana_up"
        },
    "mana_up_2":
        {
            "stat_up": {"stat": "max_mana", "value": 15},
            "name": "mana_up"
        },
    "mana_up_3":
        {
            "stat_up": {"stat": "max_mana", "value": 20},
            "name": "mana_up"
        },
}
