extends Control

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if g.banners:
        g.banners.show_banner(false)
    var dir = Directory.new()
    $Buttons/Continue.disabled = !dir.file_exists("user://save.save")

func _on_Continue_pressed() -> void:
    $Loading.show()
    g.wait_and_execute(0.2, g, "load_save")
#    g.load_save()

func _on_New_pressed() -> void:
    if $Buttons/Continue.disabled:
#        $Sure.hide()
        $Loading.show()
        g.wait_and_execute(0.2, g, "new")
#        g.new()
    else:
        $Sure.show_modal(true)

func _on_Yes_pressed() -> void:
#    g.new()
    $Sure.hide()
    $Loading.show()
    g.wait_and_execute(0.2, g, "new")

func _on_No_pressed() -> void:
    $Sure.hide()

func _on_SettingsBack_pressed() -> void:
    $Settings.hide()
    $Buttons.show()

func _on_Settings_pressed() -> void:
    $Settings.show()
    $Buttons.hide()
