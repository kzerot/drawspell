extends Node
var PlayerClass = load("res://player.gd")
# Because of cyclic load

var player

var effects = {}
var dungeon_data = {"level": 0}
const MAX_LEVEL = 20
var max_level = 0
var won = false
var banners = null
var show_banners = true
var remove_banner_price = 0
var first_time = {"town":true, "dungeon":true}

func _ready() -> void:
    player = PlayerClass.new()
    banners = load("res://Banners.tscn").instance()
    add_child(banners)
    if banners.ready:
        banners.show_banner(false)
    for a in get_method_list():
        if a.name == 'iap_has_purchased':
            print(a)

    var t = get_tree().create_timer(1)
    t.connect("timeout", self, "load_iap")

func load_iap():
    #First listen to the sku details update callback
    IAP.connect("sku_details_complete",self,"sku_details_complete")

    #Then ask google the details for these items
    IAP.sku_details_query(["remove_banner"]) #pid1 and pid2 are our product ids entered in Googleplay dashboard
    #Add a listener first
    IAP.connect("has_purchased",self,"iap_has_purchased")
    IAP.request_purchased() #Ask Google for all purchased items
    #First listen for purchase_success callback
    IAP.connect("purchase_success",self,"purchase_success_callback")

func buy_ad_disable():
    IAP.purchase("remove_banner")

func on_purchase_fail():
    print("MAGIC: PURSUASE FAILED")

func purchase_success_callback(item_name):
    print("MAGIC: purchase success, OK  ", item_name)
    if item_name == "remove_banner":
        show_banners = false

func iap_has_purchased(item_name):
    if item_name:
        print("MAGIC: Already pursuazed ", item_name)
        if item_name == "remove_banner":
    #        IAP.consume_all()
            show_banners = false

func sku_details_complete():
    print("MAGIC: Details complete")
    if 'remove_banner' in IAP.sku_details:
        remove_banner_price = IAP.sku_details["remove_banner"].price

func save(where):
    var file = File.new()
    if file.open("user://save.save", File.WRITE) == OK:
        var dict = {"player": player.pack(), "where": where, "won":  won}
        if dungeon_data:
            dict.dungeon_data = dungeon_data
        dict.first_time = first_time
        dict.max_level = max_level
        file.store_string(to_json(dict))
        file.close()

func use_potion(potion):
    var message = "OK"
    var result_effects = []
    var dur = potion.data.duration if "duration" in potion.data else 0
    for p in player.get_potions():
        if p.name == potion.name and p.count > 0:
            var used = false

            for effect in p.data.effects:
                print(p.data.effects)
                match effect:
                    "restore_hp":
                        if player.hp < player.max_hp:
                            player.hp = int(clamp(player.hp + p.data.effects["restore_hp"] , 0, player.max_hp))
                            used = true
                        else:
                            message = "HPisFull"
                    "restore_mana":
                        if player.mana < player.max_mana:
                            player.mana = int(clamp(g.player.mana + p.data.effects["restore_mana"], 0, player.max_mana))
                            used = true
                        else:
                            message = "MPisFull"
                    "regen_hp", "regen_mana":
                        result_effects.append({"type": effect, "value": p.data.effects[effect].value, "duration": dur})
                        used = true
                    "stat_up":
                        result_effects.append({"type": "pot_buff", "stat_up": p.data.effects[effect], "duration": dur})
                        used = true
            if used:
                p.count -= 1
            break
    return {"message": message, "effects": result_effects}

func wait_and_execute(time, who, function, args=[]):
    var t = get_tree().create_timer(time)
    t.connect("timeout", who, function, args)

func load_save():
    dungeon_data = {"level": 0}
    var file = File.new()
    if file.open("user://save.save", File.READ) == OK:
        var content = parse_json(file.get_as_text())
        if "first_time" in content:
            first_time = content.first_time
        if "won" in content:
            won = content.won
        if "player" in content:
            player.unpack(content.player)
        if "dungeon_data" in content:
            dungeon_data = content.dungeon_data
        if "max_level" in content:
            max_level = content.max_level
        file.close()
        if content and "where" in content:
            if content.where == "town":
                get_tree().change_scene("res://Town.tscn")
            else:
                get_tree().change_scene("res://Game.tscn")
        else:
            get_tree().change_scene("res://Town.tscn")

func new():
    randomize()
    first_time = {"town":true, "dungeon":true}
    max_level = 0
    player = Player.new()
    dungeon_data = {"level": 0}
    save("town")
    get_tree().change_scene("res://Town.tscn")

func town():
    if g.banners:
        g.banners.show_banner(false)
    get_tree().change_scene("res://Town.tscn")
    save("town")

func dungeon(lvl):
    if g.banners:
        g.banners.show_banner(false)
    dungeon_data = {"level": lvl}

    player.hp = player.max_hp
    player.mana = player.max_mana
    get_tree().change_scene("res://Game.tscn")

func deeper():
    if dungeon_data and "level" in dungeon_data:
        if dungeon_data.level+1 > max_level:
            max_level = dungeon_data.level+1
        dungeon(dungeon_data.level+1)
    else:
        dungeon(0)

func prepare_player():
    player.effects = []

func recalc_player():
    # For base values
    var dummy_player = PlayerClass.new()
    var changed_stats = []
    for _upgrade in player.upgrades:
        var upgrade = skills.skills[_upgrade]
        if "add_spell" in upgrade:
            effects[upgrade.name] = upgrade.add_spell
        elif "stat_up" in upgrade:
            dummy_player.set(upgrade.stat_up.stat, dummy_player.get(upgrade.stat_up.stat) + upgrade.stat_up.value)
            changed_stats.append(upgrade.stat_up.stat)
    for c in changed_stats:
        player.set(c, dummy_player.get(c))

func compare_weapon(w1, w2):
    if not w1 or not w2 or w1.type != w2.type:
        return false

    for stat in w1:
        if typeof(w1[stat]) == TYPE_DICTIONARY:
            for r in w1[stat]:
                if stat in w2 and r in w2[stat] and w1[stat][r] != w2[stat][r]:
                    return false
        elif w1[stat] != w2[stat]:
            return false
    return true
