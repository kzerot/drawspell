extends Spatial
tool
var _count = 3

export (int) var count setget set_count, get_count

func set_count(val):
    if has_node("Sprite"):
        $Sprite.get_surface_material(0).set_shader_param("count", val)

func get_count():
    if has_node("Sprite"):
        return $Sprite.get_surface_material(0).get_shader_param("count")
