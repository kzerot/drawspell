extends Spatial
tool

export(NodePath) var _ball
export(NodePath) var _explosion

var ball
var explosion_effect

signal hit

func _ready() -> void:
    if has_node(_ball):
        ball = get_node(_ball)
    if has_node(_explosion):
        explosion_effect = get_node(_explosion)

    if explosion_effect:
        explosion_effect.emitting = false
    if ball:
        ball.translation = Vector3(0,0,0)
        ball.emitting = true

func fire():
    $AnimationPlayer.play("Fire")

func explosion():
    if ball:
        ball.emitting = false
    if explosion_effect:
        explosion_effect.emitting = true
    print("Emit fireball")



func end():
    if not Engine.editor_hint:
        emit_signal("hit")


func die():
    self.queue_free()
#    pass
