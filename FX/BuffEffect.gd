extends Spatial

func fire():
    $AnimationPlayer.play("Create")
    $AnimationPlayer.seek(0)

func die():
    $AnimationPlayer.play("End")

func remove_self():
    self.queue_free()
