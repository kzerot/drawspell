extends Spatial

tool


export (bool) var big setget set_big, get_big

func set_big(val):
    if has_node("1"):
        $"3".visible = val
        $"4".visible = val
        $"6".visible = val

func get_big():
    if has_node("1"):
        return $"3".visible
