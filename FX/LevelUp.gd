extends Control

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var selected
var player
# Called when the node enters the scene tree for the first time.
signal recalc_player

func _ready() -> void:
    for c in $Control/Image.get_children():
        c.connect("pressed", self, "skill_selected", [c])
        c.set_available()
    $Info/Commit.connect("pressed", self, "commit")


func init(_player):
    player = _player
    $Exp.text = str(floor(player.experience)) + " Exp"
    check()

func upd_exp():
    $Exp.text = str(floor(player.experience)) + " Exp"

func check():
    for c in $Control/Image.get_children():
        if c.upgrade_name in player.upgrades:
            c.set_clickable()
        elif not c.parent:
            c.set_available()
        elif get_node("Control/Image").get_node(c.parent).enabled:
            c.set_available()
        else:
            c.set_disable()


func skill_selected(skill):
    selected = skill
    var skill_data = skills.skills[selected.upgrade_name]
    $Info.show_modal()
    $Info/Cont/Image.hide()
    var spell_name = skill_data.name
    $Info/Name.text = spell_name
#    $Info/Cont/Descr/Descr.text = "Description of "+ skill_data.name
    if ResourceLoader.exists("res://UI/Symbols/" + spell_name+".png"):
        $Info/Cont/Image.texture = load("res://UI/Symbols/" + spell_name+".png")
        $Info/Cont/Image.show()

    var descr = ""
    if "add_spell" in skill_data:
        if "replace" in skill_data:
            descr+= tr("UpgradePL") + " " + tr(spell_name)
        else:
            descr += tr("New spell")
        if "damage" in skill_data.add_spell:
            descr += "\n" + tr("Damage") + ": "
            var i = 0
            for d in skill_data.add_spell.damage:
                descr += str(skill_data.add_spell.damage[d]) + " " + tr(d)
                i += 1
                if i < skill_data.add_spell.damage.size():
                    descr+= ", "
        if "stat_up" in skill_data.add_spell:
#            descr += "\n"
#            var i = 0
            for d in skill_data.add_spell.stat_up:
                descr += "\n" + tr(d) + " +" + str(skill_data.add_spell.stat_up[d])

        if "effect" in skill_data.add_spell:
            match skill_data.add_spell.effect.type:
                "stun":
                    descr += "\n" + tr("stun_descr") % str(skill_data.add_spell.effect.duration)
                "dot":
                    for d in skill_data.add_spell.effect.damage:
                        descr += "\n" + tr("dot") % [tr(d), str(skill_data.add_spell.effect.damage[d]),
                                                     str(skill_data.add_spell.effect.duration)]

        if skill_data.add_spell.type == "expertise":
            descr += "\n" + tr("expertise_descr")

        if "duration" in skill_data.add_spell:
            descr+= "\n" + tr("Duration")+": " + str(skill_data.add_spell.duration)+tr("s")

        if "mana" in skill_data.add_spell:
            descr+= "\n" + tr("Mana")+": " + str(skill_data.add_spell.mana)

    if "stat_up" in skill_data:
        descr += tr(skill_data.stat_up.stat) + " +" + str(skill_data.stat_up.value)


    $Info/Cont/Descr/Descr.text = descr
    $Info/Exp.text = str(floor(selected.experience))+ " Exp"
    if selected.parent:
        $Info/Commit.visible = get_node("Control/Image").get_node(selected.parent).enabled and \
                               not skill.upgrade_name in player.upgrades and selected.experience <= player.experience
    else:
        $Info/Commit.visible = not skill.upgrade_name in player.upgrades and selected.experience <= player.experience


func commit():
    if selected:
#        selected.set_clickable()
        $Info.hide()
        player.upgrades.append(selected.upgrade_name)
        player.experience -= selected.experience
        $Exp.text = str(floor(player.experience)) + " Exp"
        check()


func _on_Back_pressed() -> void:
    emit_signal("recalc_player")
    hide()
